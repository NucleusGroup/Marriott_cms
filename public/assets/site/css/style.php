<?php
header('Content-type: text/css');
?>
body {
font-family: 'Open Sans', sans-serif;    
width: 100%;

background: url(<?php echo $_GET['path']; ?>) no-repeat center center fixed; 
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
margin-bottom: 0 !important;
padding-bottom: 0 !important;
background-color: #000;

}

body:before {
    content: "";
    position: absolute;
    width : 100%;
    min-height: 100%;
    background: inherit;
    z-index: -1;
   
}

body::after {
    content: "";
    background-color: #9932CC;
    opacity: 0.1;
    display:block !important;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: fixed;
    z-index: -1;
}

