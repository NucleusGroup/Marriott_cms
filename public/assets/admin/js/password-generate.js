$.extend({
    password: function (length, special) {
        var iteration = 0;
        var password = "";
        var randomNumber;
        if (special == undefined) {
            var special = false;
        }
        while (iteration < length) {

            randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
            if (!special) {
                if ((randomNumber >= 33) && (randomNumber <= 47)) {
                    continue;
                }
                if ((randomNumber >= 58) && (randomNumber <= 64)) {
                    continue;
                }
                if ((randomNumber >= 91) && (randomNumber <= 96)) {
                    continue;
                }
                if ((randomNumber >= 123) && (randomNumber <= 126)) {
                    continue;
                }
            }
            iteration++;
            password += randomNumber;
        }
        return password;
    }
});
$(document).ready(function () {
    $('.link-password').click(function (e) {
        linkId = $(this).attr('id');
        if (linkId == 'generate') {
            password = $.password(4, true);
            $('#random').empty().hide().append(password).fadeIn('slow');
            $('#confirm').fadeIn('slow');
            $('#password').val(password);
            $('#password_confirmation').val(password);
            $(this).hide();
        }
        e.preventDefault();
    });
});