$(document).ready(function ($) {

    $("body").on("click", ".delete-user", function (e) {
        e.stopPropagation();
        if (confirm('Are you sure you want to delete this user?'))
            return true;
        return false;
    });
    $("body").on("click", ".delete-file", function (e) {
        e.stopPropagation();
        if (confirm('Are you sure you want to delete this file?'))
            return true;
        return false;
    });
    $("body").on("click", ".delete-hotel", function (e) {
        e.stopPropagation();
        if (confirm('Are you sure you want to delete this hotel?'))
            return true;
        return false;
    });
    $("body").on("click", ".delete", function (e) {
        e.stopPropagation();
        if (confirm('Are you sure you want to delete this item?'))
            return true;
        return false;
    });
});