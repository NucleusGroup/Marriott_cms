<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('days_statistic', function(Blueprint $table) {
            $table->increments('id');
            $table->string('row');
            $table->string('actuals_dbd');
            $table->string('name');
            $table->string('value');
            $table->string('date');           
            $table->unsignedInteger('hotel_id')->nullable();
            $table->foreign('hotel_id')->references('id')->on('hotel')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('days_statistic');
    }

}
