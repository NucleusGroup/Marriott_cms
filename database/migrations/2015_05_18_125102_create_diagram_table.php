<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagramTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('diagrams', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('x_y');
                        $table->string('date');
                        $table->string('actuals');
                        $table->string('otb');
                        $table->string('yesterday');
                        $table->string('pick_up');
                        $table->string('lost');
                        $table->string('fcst');
                        $table->string('re_fcst');
                        $table->unsignedInteger('hotel_id')->nullable();
			$table->foreign('hotel_id')->references('id')->on('hotel')->onDelete('set null');
                        
                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('diagrams');
	}

}
