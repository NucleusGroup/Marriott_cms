<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('day', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('import_date');
                        $table->string('name');
                        $table->string('row');			
			$table->string('day');			
                        $table->string('mtd');			
                        $table->string('fcstmtd');			
                        $table->string('fcst');			
                        $table->string('yesterday');			
                        $table->string('last_year');			
                        $table->string('budget');
                        $table->unsignedInteger('hotel_id')->nullable();
			$table->foreign('hotel_id')->references('id')->on('hotel')->onDelete('set null');
                        $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('day');
	}

}
