<?php

namespace App\Services;

use Maatwebsite\Excel\Facades\Excel;
use App\Day;
use App\Diagram;

class ImportDeleteService
{

    public function deleteRecords($path, $hotelId, $isError)
    {
        Excel::load($path, function($reader) use ($hotelId, &$isError) {
            $numberOfSheets = $reader->getSheetCount();
            //insert daily data into database
            $sheet = $reader->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $importDate = $sheet->getCell('B1')->getFormattedValue();
            if (empty($importDate)) {
                $day = $sheet->getCell('D3')->getValue();
                $day = str_pad($day, 2, '0', STR_PAD_LEFT);
                $month = $sheet->getCell('E3')->getValue();
                $month = str_pad($month, 2, '0', STR_PAD_LEFT);
                $year = $sheet->getCell('D1')->getValue();

                if (empty($day) || empty($month) || empty($year)) {
                    $isError = true;
                    return false;
                }
                $importDate = $day . '-' . $month . '-' . $year;
                $date = date_format(date_create($importDate), "Y-m-d");
            } else {
                $newDate = ($importDate - 25569) * 86400;
                $date = gmdate("Y-m-d", $newDate);
            }
            // check if exists.            
            Day::where('hotel_id', $hotelId)->where('import_date', $date)->delete();
            Diagram::where('hotel_id', $hotelId)->where('import_date', $date)->delete();
        });
    }

    public function deletePath($filePath , $id)
    {
        unlink($filePath);
        $dirPath = public_path() . "/files/" . $id;

        // delete directory
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }
    }

}
