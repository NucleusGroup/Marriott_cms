<?php namespace App;


use Illuminate\Database\Eloquent\Model;

class AddUserToHotel extends Model {

    protected $table = 'hotel_level';
    public $timestamps = false;
    
    public function users() {
        $this->belongsTo('User', 'user_id');
    }
    
    public function hotels() {
        $this->belongsTo('Hotel', 'hotel_id');
    }
    
    public function levels() {
        $this->belongsTo('Level', 'level_id');
    }
    
   

}
