<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model {

     protected $table = 'hotel';
     
     protected $fillable = array('position', 'name', 'description', 'image', 'city', 'country', 'number_of_rooms', 'meeting_rooms', 'link');
     protected $appends = array(
         'imagePath',
     );
     public $timestamps = false;

     public function getImagePathAttribute() {
         $imagePath = public_path() .'/appfiles/hotel/' . $this->id . '/'. $this->image;
         if(file_exists($imagePath)) {
             return $this->getAssetsPath('/'. $this->image);
         }
     }     
      private function getAssetsPath($file) {
        return asset('/appfiles/hotel/' . $this->id . $file);
    }
}
