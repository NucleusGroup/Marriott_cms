<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use App\Hotel;
use App\AddUserToHotel;
use App\Day;
use App\Diagram;
use App\DayStats;
use App\Upload;
use App\Files;
use Datatables;
use Lang;
use Input;
use Image;
use App\Http\Requests\Admin\HotelRequest;

class HotelController extends AdminController {

    protected $hotel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Hotel $hotel) {
//		$this->middleware('auth');		

        $this->hotel = $hotel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $hotels = Hotel::all();
        return view('admin/hotel/index', compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('admin.hotel.create_edit', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(HotelRequest $request) {

        $image = Input::file('image');
        $hotel = new Hotel();
        $hotel->name = $request->name;
        $hotel->description = $request->description;
        $hotel->number_of_rooms = $request->number_of_rooms;
        $hotel->meeting_rooms = $request->meeting_rooms;
        $europe = explode(',', Input::get('city'));
        if (count($europe) > 1) {
            $hotel->city = $europe[0];
            $hotel->country = $europe[1];
        }
        if (strpos(Input::get('link'), 'http://') !== false) {
            $hotel->link = $request->link;
        } else {
            $hotel->link = 'http://' . $request->link;
        }
        $hotel->sqr_mtr = $request->sqr;
        $date = str_replace('/', '-', $request->openingDate);
        $hotel->opening_date = date("Y-m-d", strtotime($date));
        $hotel->image = 'main_image.' . $image->getClientOriginalExtension();
        $hotel->save();

        if (Input::hasFile('image')) {
            $file = Input::file('image');
            $imgPth = public_path() . '/appfiles/hotel/' . $hotel->id . '/';
            $imgName = 'main_image.' . $image->getClientOriginalExtension();
            $file->move($imgPth, $imgName);

            // crop image

            $img = Image::make('appfiles/hotel/' . $hotel->id . '/' . $hotel->image);
            $img->fit(1500, 200);
            $img->save($imgName);

           
            // blur image
            $imgB = Image::make('appfiles/hotel/' . $hotel->id . '/' . $hotel->image);    
            $imgB->blur(40);
            $imgNameB = 'main_image_blur.jpg';
            $imgB->save($imgPth . $imgNameB);
        }

        return redirect('admin/hotels')->with('success', Lang::get('admin/hotel.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($hotel) {
        return $hotel;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $hotel = Hotel::find($id);
        return view('admin.hotel.create_edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(HotelRequest $request, $id) {

        $image = Input::file('image');

        $hotel = Hotel::find($id);
        $hotel->name = $request->name;
        $hotel->description = $request->description;
        $hotel->number_of_rooms = $request->number_of_rooms;
        $hotel->meeting_rooms = $request->meeting_rooms;
        $hotel->description = $request->description;
        $europe = explode(',', Input::get('city'));

        if (count($europe) > 1) {
            $hotel->city = $europe[0];
            $hotel->country = $europe[1];
        }
        if (strpos(Input::get('link'), 'http://') !== false) {
            $hotel->link = $request->link;
        } else {
            $hotel->link = 'http://' . $request->link;
        }

        $hotel->sqr_mtr = $request->sqr;
        $date = str_replace('/', '-', $request->openingDate);
        $hotel->opening_date = date("Y-m-d", strtotime($date));
        if (!empty($image)) {
            $hotel->image = 'main_image.' . $image->getClientOriginalExtension();
        }
        $hotel->save();


        if (Input::hasFile('image')) {
            // delete old picture
            $dirPath = public_path() . '/appfiles/hotel/' . $id;
            $imgPath = $dirPath . '/' . $hotel->image;
            $blurImage = $dirPath . '/main_image_blur.jpg';
            if (file_exists($imgPath)) {
                unlink($imgPath);
            }
            if (file_exists($blurImage)) {
                unlink($blurImage);
            }

            $file = Input::file('image');
            $imgPth = public_path() . '/appfiles/hotel/' . $hotel->id . '/';
            $imgName = 'main_image.' . $image->getClientOriginalExtension();
            $file->move($imgPth, $imgName);

            // crop image

            $img = Image::make('appfiles/hotel/' . $hotel->id . '/' . $hotel->image);
            $img->fit(1500, 200);            
            $img->save($imgPth . $imgName);
            
            
            // blur image
            $imgB = Image::make('appfiles/hotel/' . $hotel->id . '/' . $hotel->image);    
            $imgB->blur(40);
            $imgNameB = 'main_image_blur.jpg';
            $imgB->save($imgPth . $imgNameB);
            
        }

        return redirect('admin/hotels')->with('success', Lang::get('admin/hotel.edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        Day::where('hotel_id', $id)->delete();
        Diagram::where('hotel_id', $id)->delete();
        DayStats::where('hotel_id', $id)->delete();
        AddUserToHotel::where('hotel_id', $id)->delete();
        $uploads = Upload::where('hotel_id', $id)->get();
        foreach ($uploads as $upload) {
            $file = Files::where('entity_id', $upload->id)->first();
            $dirPath = public_path() . '/files/' . $file->id;
            $filePath = $dirPath . '/' . $file->name;
            if (file_exists($filePath)) {
                unlink($filePath);
            }
            if (is_dir($dirPath)) {
                rmdir($dirPath);
            }
            $file->delete();
            $upload->delete();
        }

        $hotel = Hotel::find($id);
        $dirPath = public_path() . '/appfiles/hotel/' . $id;
        $imgPath = $dirPath . '/' . $hotel->image;
        $blurImage = $dirPath . '/main_image_blur.jpg';
        if (file_exists($imgPath)) {
            unlink($imgPath);
        }
        if (file_exists($blurImage)) {
            unlink($blurImage);
        }
        $hotel->delete();

        //remove folder
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }
        return back()->with('success', Lang::get('admin/hotel.delete'));
    }

    public function data() {
        $hotels = Hotel::select(array('*'));

        return Datatables::of($hotels)
                        ->add_column('actions', '<a href="{{{ URL::to(\'admin/hotel/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-pencil"></span>  Edit</a>
                    <a href="{{{ URL::to(\'admin/hotel/\' . $id . \'/delete\' ) }}}" class="delete-hotel btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                ')
                        ->remove_column('id', 'description', 'image', 'position', 'imagePath', 'country', 'city', 'number_of_rooms', 'meeting_rooms', 'link')
                        ->make();
    }

}
