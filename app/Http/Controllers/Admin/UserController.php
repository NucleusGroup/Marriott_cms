<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Hotel;
use App\AddUserToHotel;
use Lang;
use Mail;
use Input;
use App\Level;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;

class UserController extends AdminController {
    /*
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index() {
        // Show the page
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        $levels = Level::all();
        $hotels = Hotel::all();

        return view('admin.users.create_edit', compact('levels', 'hotels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request) {
        $levels = Input::get('level');
        $hotels = Input::get('hotel');
        $checkbox = Input::get('sendEmail');


        if (count(array_unique($hotels)) < count($hotels)) {
            return back()->with('error', Lang::get('admin/users.crud.duplicate'));
        }


        $user = new User ();
        $user->name = ucfirst($request->name);
        $user->username = md5($request->name . time());
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->confirmation_code = str_random(32);
        $user->confirmed = $request->confirmed;
        $user->save();


        for ($i = 0; $i < count($hotels); $i++) {
            $levelUser = new AddUserToHotel();

            $levelUser->level_id = $levels[$i];
            $levelUser->hotel_id = $hotels[$i];
            $levelUser->user_id = $user->id;
            $levelUser->save();
        }
        $password = Input::get('password');

        // SEND USER EMAIL
        if ($checkbox) {
            Mail::send('emails.password', array('password' => $password, 'name' => $user->name, 'email' =>  $user->email ), function ($message) use ($user) {
                $message->from('info@marriotthotels.no', 'Nordic Hospitality Performance | Login credentials');
                $message->to($user->email);
                $message->subject('Login credentials');
            });            
        }

        return redirect('admin/users')->with('success', Lang::get('admin/users.crud.create'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {
        $levels = Level::all();
        $hotels = Hotel::all();
        $hotelsUser = AddUserToHotel::where('user_id', $id)->get();
       
        $user = User::find($id);
        return view('admin.users.create_edit', compact('user', 'levels', 'hotels', 'hotelsUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id) {
        $levels = array_filter(Input::get('level'));
        $hotels = array_filter(Input::get('hotel'));
        $checkbox = Input::get('sendEmail');
        if (count(array_unique($hotels)) < count($hotels)) {
            return back()->with('error', Lang::get('admin/users.crud.duplicate'));
        }
        $user = User::find($id);
        $user->name = ucfirst($request->name);
        $user->confirmed = $request->confirmed;
        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user->password = bcrypt($password);
            }
        }
        $user->save();

        for ($i = 0; $i < count($hotels); $i++) {

            $check = AddUserToHotel::where('hotel_id', $hotels[$i])->where('user_id', $id)->first();

            if (count($check) > 0) {
                $check->level_id = $levels[$i];
                $check->hotel_id = $hotels[$i];
                $check->user_id = $user->id;
                $check->save();
            } else {
                $levelUser = new AddUserToHotel();
                $levelUser->level_id = $levels[$i];
                $levelUser->hotel_id = $hotels[$i];
                $levelUser->user_id = $user->id;
                $levelUser->save();
            }
        }

        $password = Input::get('password');

        // SEND USER EMAIL
        if ($checkbox) {           
            Mail::send('emails.password', array('password' => $password, 'email' => $user->email, 'name' => $user->name), function ($message) use ($user) {
                $message->from('info@marriotthotels.no', 'Nordic Hospitality Performance | Login credentials');
                $message->to($user->email);
                $message->subject('Login credentials');
            });
        }

        return redirect('admin/users')->with('success', Lang::get('admin/users.crud.edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($id) {
        $user = User::find($id);
        // Show the page
        return view('admin.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($id) {
        AddUserToHotel::where('user_id', $id)->delete();
        $user = User::find($id);
        $user->delete();
        return redirect('admin/users')->with('success', Lang::get('admin/users.crud.delete'));
    }

    public function deleteHotel($id) {
        AddUserToHotel::find($id)->delete();

        return back()->with('success', Lang::get('admin/users.crud.hotel-delete'));
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data() {
        $users = User::select(array('users.id', 'users.name', 'users.email', 'users.confirmed', 'users.created_at'));

        return Datatables::of($users)
                        ->edit_column('confirmed', '@if ($confirmed=="1") <span class="glyphicon glyphicon-ok"></span> @else <span class=\'glyphicon glyphicon-remove\'></span> @endif')
                        ->add_column('actions', '@if ($id!="1")<a href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="delete-user btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                @endif')
                        ->remove_column('id')
                        ->make();
    }

}
