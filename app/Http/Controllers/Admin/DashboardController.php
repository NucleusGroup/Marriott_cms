<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Upload;
use App\Hotel;
use App\User;

class DashboardController extends AdminController {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $title = "Dashboard";

        
        $users = User::count();
        $upload = Upload::count();
        $hotels = Hotel::count();
       
		return view('admin.dashboard.index',  compact('title','upload','hotels', 'users'));
	}
}