<?php

namespace App\Http\Controllers;

use App\AddUserToHotel;
use App\Hotel;
use App\Day;
use Response;
use App\Diagram;
use Session;
use Auth;
use Input;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    protected $hotel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Hotel $hotel)
    {
        //$this->middleware('auth');
        //parent::__construct();

        $this->hotel = $hotel;
        //$this->user = $user;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {

        $hotels = Hotel::all();
        return view('pages.home', compact('hotels'));

        //return view('pages.welcome');
    }

// ACTUAL YESTERDAY
    public function getStats()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $check = Day::where('hotel_id', $id)->get();

            if (count($check) > 0 && count($level) > 0) {
                $dates = Day::allDates($id);
                $lD    = Day::lastDay($id);

                $date = \Input::get('date');
                if ($date) {
                    $lD = \Input::get('date');
                    Session::set('date', $lD);
                }

                $curentDate = strtotime(date('d-m-Y'));
                $curentTime = time();
                $time       = strtotime('10:00:00');

                if (strtotime($lD) == $curentDate && count(Day::allDates($id)) > 1) {
                    if ($curentTime < $time) {
                        $lD  = Day::secondHighest($id);
                        $day = Day::where('hotel_id', $id)->where('import_date', $lD)->orderBy('import_date')->get();
                        if (count(Day::allDates($id)) > 3) {
                            $dayBefore = Day::dayBefore($id, $lD);
                            $dB        = Day::dayBefore($id, $lD);
                        } else {
                            $dayBefore = null;
                        }
                    } else {
                        $day       = Day::where('hotel_id', $id)->where('import_date', $lD)->orderBy('import_date')->get();
                        $dayBefore = Day::dayBefore($id, $lD);
                        $dB        = Day::dayBefore($id, $lD);
                    }
                } else {
                    $day = Day::where('hotel_id', $id)->where('import_date', $lD)->orderBy('import_date')->get();
                    $dB  = Day::dayBefore($id, $lD);
                    if ($dB) {
                        $dayBefore = Day::dayBefore($id, $lD);
                        $dB        = $dB->import_date;
                    } else {
                        $dayBefore = null;
                    }
                }
                // RENT
                $rent = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RE')->sum('day');

                // OCC
                $av = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('day');
                if ($av == '0') {
                    $occ = 0;
                } else {
                    $st  = Day::occ($id, $lD, 'day');
                    $occ = $st / $av;
                }
                // OCC FCST
                $avFcst = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('fcst');
                if ($avFcst == '0') {
                    $occFcst = 0;
                } else {
                    $stFcst  = Day::occ($id, $lD, 'fcst');
                    $occFcst = $stFcst / $avFcst;
                }




                // ADR and FCST
                $adr     = Day::adr($id, $lD, 'day') / $st;
                $adrFcst = Day::adr($id, $lD, 'fcst') / $stFcst;

                // REVRAR
                $revPar     = $occ * $adr;
                $revParFcst = $occFcst * $adrFcst;

                // PICK UP TRANSIENT
                $transientPickup = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OTT')->sum('day') -
                        Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OTT')->sum('yesterday');

                // PICK UP GROUP
                $groupPickup      = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OGT')->sum('day') -
                        Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OGT')->sum('yesterday');
                // TOTAL REVENUE
                $totalRevenue     = Day::totalRevenue($id, $lD, 'day');
                $totalRevenueDay     = Day::totalRevenue($id, $lD, 'day') / $av;
                $totalRevenueFcst = Day::totalRevenue($id, $lD, 'fcst') / $avFcst;

                // LABOR EFFICIENCY
//                $row124 = Day::where("row", "124")->where('import_date', $lD)->where('hotel_id', $id)->first()->day;
//                if ($row124 == '0') {
//                    $laborEfficiency = 0;
//                } else {
//                    $laborEfficiency = Day::where("row", "127")->where('import_date', $lD)->where('hotel_id', $id)->first()->day / $row124;
//                }
                $laborEfficiency = Day::where("row", "124")->where('import_date', $lD)->where('hotel_id', $id)->first()->day;
                $d128            = Day::where("row", "128")->where('import_date', $lD)->where('hotel_id', $id)->first()->day;
                $d126            = Day::where("row", "126")->where('import_date', $lD)->where('hotel_id', $id)->first()->day;

                // DAY BEFORE
                if ($dayBefore) {

                    $rentY = Day::where('hotel_id', $id)->where('import_date', $dB)->where('code', 'RE')->sum('day');
//                    if (!empty($rentY)) {
//                        $rentY = $rentY->rent;
//                    }
                    // OCC
                    $avY   = Day::where('code', 'AV')->where('import_date', $dB)->where('hotel_id', $id)->sum('day');
                    if ($avY == '0') {
                        $occY = 0;
                    } else {
                        $stY  = Day::occ($id, $dB, 'day');
                        $occY = $stY / $avY;
                    }



//                    $avFcst = Day::where('code', 'AV')->where('import_date', $dB)->where('hotel_id', $id)->sum('fcst');
//                    if ($avFcst == '0') {
//                        $occFcst = 0;
//                    } else {
//                        $stFcst  = Day::occ($id, $dB, 'fcst');
//                        $occFcst = $stFcst / $avFcst;
//                    }
                    // ADR
                    $adrY = Day::adr($id, $dB, 'day') / $stY;
//                    $adrFcst = Day::adr($id, $dB, 'fcst') / $stFcst;
                    // REVRAR
                    $revParY = $occY * $adrY;
//                    $revParFcst = $occFcst * $adrFcst;
                    // PICK UP TRANSIENT
                    $transientPickupY = Day::where('hotel_id', $id)->where('import_date', $dB)->where('code', 'OTT')->sum('day') -
                            Day::where('hotel_id', $id)->where('import_date', $dB)->where('code', 'OTT')->sum('yesterday');

                    // PICK UP GROUP
                    $groupPickupY     = Day::where('hotel_id', $id)->where('import_date', $dB)->where('code', 'OGT')->sum('day') -
                            Day::where('hotel_id', $id)->where('import_date', $dB)->where('code', 'OGT')->sum('yesterday');
                    // TOTAL REVENUE
                    $totalRevenueY    = Day::totalRevenue($id, $dB, 'day') / $avY;
//                    $totalRevenueFcst = Day::totalRevenue($id, $dB, 'fcst') / $avFcst;
                    // LABOR EFFICIENCY
//                    $row124Y = Day::where("row", "124")->where('import_date', $dB)->where('hotel_id', $id)->first()->day;
//                    if ($row124Y == '0') {
//                        $laborEfficiencyY = 0;
//                    } else {
//                        $laborEfficiencyY = Day::where("row", "127")->where('import_date', $dB)->where('hotel_id', $id)->first()->day / $row124Y;
//                    }
                    $laborEfficiencyY = Day::where("row", "124")->where('import_date', $dB)->where('hotel_id', $id)->first()->day;
//                    $d128             = Day::where("row", "128")->where('import_date', $dB)->where('hotel_id', $id)->first()->day;
//                    $d126             = Day::where("row", "126")->where('import_date', $dB)->where('hotel_id', $id)->first()->day;
                } // END DAY BEFORE


                return view('pages.stats', compact('rent', 'rentY', 'lD', 'level', 'hotel', 'day', 'dayBefore', 'occ', 'adr', 'revPar', 'transientPickup', 'groupPickup', 'totalRevenue', 'laborEfficiency'
                                , 'dayY', 'occY', 'adrY', 'revParY', 'transientPickupY', 'groupPickupY', 'totalRevenueY', 'laborEfficiencyY', 'dates', 'occFcst', 'adrFcst', 'revParFcst', 'totalRevenueFcst', 'd128', 'd126', 'totalRevenueDay'));
            }

            return view('pages.empty', compact('hotel'));
        } else {
            return redirect('user/home');
        }
    }

    public function redirect($hotel)
    {
        Session::set('hotel', $hotel->id);
        return redirect()->action('HomeController@getStats');
    }

// REVENUE
    public function getRevenue()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('20:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }
            $transient  = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RT')->sum('day');
            $group      = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RG')->sum('day');
            $contract   = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RC')->sum('day');
            $roomsOther = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'RP')->orWhere('code', 'OR')
                                ->orWhere('code', 'NS')->orWhere('code', 'CF');
                    })->sum('day');
            $totalRooms = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')
                                ->orWhere('code', 'RP')->orWhere('code', 'OR')->orWhere('code', 'NS')->orWhere('code', 'CF');
                    })->sum('day');
            $breakfast       = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PB')->sum('day');
            $lunch           = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PL')->sum('day');
            $dinner          = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PD')->sum('day');
            $foodOther       = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PO')->sum('day');
            $nab             = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'BN')->sum('day');
            $beer            = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'BB')->sum('day');
            $wine            = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'BW')->sum('day');
            $liqour          = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'BL')->sum('day');
            $totalRestaurant = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'PB')->orWhere('code', 'PL')->orWhere('code', 'PD')
                                ->orWhere('code', 'PO')->orWhere('code', 'BN')->orWhere('code', 'BB')->orWhere('code', 'BW')->orWhere('code', 'BL');
                    })->sum('day');

            $banquets        = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CB')->sum('day');
            $roomRental      = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CR')->sum('day');
            $otherConference = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'CA')->orWhere('code', 'CO');
                    })->sum('day');
            $totalConference = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'CB')->orWhere('code', 'CR')
                                ->orWhere('code', 'CA')->orWhere('code', 'CO');
                    })->sum('day');
            $miniMarket = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OM')->sum('day');
            $laundry    = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OL')->sum('day');
            $parking    = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OP')->sum('day');
            $other      = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'OI')->orWhere('code', 'OT');
                    })->sum('day');

            $totalOther = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
                        $query->where('code', 'OM')->orWhere('code', 'OL')
                                ->orWhere('code', 'OP')->orWhere('code', 'OI')->orWhere('code', 'OT');
                    })->sum('day');
//            $totalRevenue2 = Day::where('hotel_id', $id)->where('import_date', $lD)->where(function($query) {
//                        $query->where('code', 'RT')->orWhere('code', 'RG')->orWhere('code', 'RC')->orWhere('code', 'RP')->orWhere('code', 'OR')->orWhere('code', 'NS')
//                                ->where('code', 'CF')->orWhere('code', 'PB')->orWhere('code', 'PL')->orWhere('code', 'PD')->orWhere('code', 'PO')->orWhere('code', 'BN')
//                                ->where('code', 'BB')->orWhere('code', 'BW')->orWhere('code', 'BL')->orWhere('code', 'CB')->orWhere('code', 'BR')->orWhere('code', 'CA')
//                                ->where('code', 'CO')->orWhere('code', 'OM')->orWhere('code', 'OL')->orWhere('code', 'OP')->orWhere('code', 'OI')->orWhere('code', 'OT');
//                    })->sum('day');
            $totalRevenue2 = Day::totalRevenue($id, $lD, 'day');

            return view('pages.revenue', compact('hotel', 'lD', 'level', 'day', 'transient', 'group', 'contract', 'roomsOther', 'totalRooms', 'breakfast', 'lunch', 'dinner', 'foodOther', 'nab', 'beer', 'wine', 'liqour', 'other', 'totalRestaurant', 'banquets', 'roomRental', 'otherConference', 'totalConference', 'miniMarket', 'laundry', 'parking', 'totalOther', 'totalRevenue2', 'dates'));
        } else {
            return redirect('user/home');
        }
    }

    // DIAGRAM
    public function getDiagram()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            if (Input::get('date')) {

                $lD     = date_format(date_create(Input::get('date')), "Y-m-d");
                $graphs = Diagram::where('hotel_id', $id)->where('import_date', $lD)->get();
                if (count($graphs) < 1) {
                    return redirect('user/home');
                }
                if (count(Day::allDates($id)) > 1) {
//                    $dates = Diagram::distinct()->selectRaw('import_date')->where('hotel_id', $id)
//                                    ->whereRaw('import_date < (SELECT MAX(import_date) FROM day)')
//                                    ->orderBy('import_date', 'desc')->get();
                    $dates = Day::allDates($id);
                } else {
                    $dates = Diagram::distinct()->select('import_date')->where('hotel_id', $id)
                                    ->groupBy('import_date')
                                    ->orderBy('import_date', 'desc')->get();
                }
            } else {
                $lD         = Day::lastDay($id);
                $curentDate = strtotime(date('d-m-Y'));
                $curentTime = time();
                $time       = strtotime('20:00:00');
                if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                    $lD     = Day::secondHighest($id);
                    $graphs = Diagram::where('hotel_id', $id)->where('import_date', $lD)->get();
                    $dates  = Diagram::distinct()->select('import_date')->where('hotel_id', $id)
                                    ->whereRaw('import_date <=  "' . $lD . '"')
                                    ->groupBy('import_date')
                                    ->orderBy('import_date', 'desc')->get();
                } else {
                    $graphs = Diagram::where('hotel_id', $id)->where('import_date', $lD)->get();
                    $dates  = Diagram::distinct()->select('import_date')->where('hotel_id', $id)
                                    ->groupBy('import_date')
                                    ->orderBy('import_date', 'desc')->get();
                }
            }



            if (count($day) > 0 && count($level) > 0) {
                $otbTramsient    = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 71 and 82")->get();
                $otbTramsientSum = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 71 and 82")->sum('day');
                $otbT            = array();
                $otbTYes         = array();
                foreach ($otbTramsient as $key => $value) {
                    $otbT[$key + 1]       = $value->day;
                    $otbTYes[$key + 1]    = $value->yesterday;
                    $otbTBudget[$key + 1] = $value->budget;
                }
                $otbGroup    = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 89 and 100")->get();
                $otbGroupSum = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 89 and 100")->sum('day');
                $otbG        = array();
                $otbGYes     = array();
                foreach ($otbGroup as $key => $value) {
                    $otbG[$key + 1]       = $value->day;
                    $otbGYes[$key + 1]    = $value->yesterday;
                    $otbGBudget[$key + 1] = $value->budget;
                }

                $budget1 = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 71 and 82")->sum('budget');
                $budget2 = Day::where('hotel_id', $id)->where('import_date', $lD)->whereRaw("row between 89 and 100")->sum('budget');

                if ($budget1 == '0' || $budget2 == '0') {
                    $budget = 0;
                } else {
                    $budget = ($otbTramsientSum + $otbGroupSum) / ($budget1 + $budget2 );
                }


                $lD = date_format(date_create($lD), "d-m-Y");

                return view('pages.diagram', compact('dates', 'lD', 'level', 'hotel', 'graphs', 'day', 'otbTramsient', 'otbGroup', 'otbT', 'otbG', 'otbTramsientSum'
                                , 'otbGroupSum', 'otbTYes', 'otbGYes', 'otbTBudget', 'otbGBudget', 'budget'));
            }

            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

// MONTH TO DATE
    public function getMtd()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('20:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }

            if (count($day) > 0 && count($level) > 0) {
                // OCC
                $av = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('mtd');
                $st = Day::occ($id, $lD, 'mtd');
                if ($av == '0') {
                    $occ = 0;
                } else {

                    $occ = $st / $av;
                }
                // ADR
                $adr          = Day::adr($id, $lD, 'mtd') / $st;
                // REVRAR
                $revPar       = $occ * $adr;
                // TOTAL REVENUE
                $totalRevenue = Day::totalRevenue($id, $lD, 'mtd');

                // LABOR EFFICIENCY
//                $row124 = Day::where("row", "124")->where('import_date', $lD)->where('hotel_id', $id)->first()->mtd;
//                if ($row124 == '0') {
//                    $laborEfficiency = 0;
//                } else {
//                    $laborEfficiency = Day::where("row", "124")->where('import_date', $lD)->where('hotel_id', $id)->first()->fcstmtd / $row124;
//                }

                $laborEfficiency = Day::where("row", "124")->where('import_date', $lD)->where('hotel_id', $id)->first()->mtd;
                // OCC FCST                
                $avFr            = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('fcstmtd');
                $stFrcs          = Day::occ($id, $lD, 'fcstmtd');
                if ($avFr == '0') {
                    $occFrcs = 0;
                } else {

                    $occFrcs = $stFrcs / $avFr;
                }


                // OCC MTD BUDGET                
                $avBdg = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('budgetmtd');
                $stBdg = Day::occ($id, $lD, 'budgetmtd');
                if ($avBdg == '0') {
                    $occBdg = 0;
                } else {
                    $occBdg = $stBdg / $avBdg;
                }


                // ADR FCST          
                if ($stFrcs !== 0) {
                    $adrFrcs = Day::adr($id, $lD, 'fcstmtd') / $stFrcs;
                } else {
                    $adrFrcs = 0;
                }


                // ADR MTD BUDGET          
                if ($stBdg !== 0) {
                    $adrBdg = Day::adr($id, $lD, 'budgetmtd') / $stBdg;
                } else {
                    $adrBdg = 0;
                }



                // REVRAR FCST
                $revParFrcs = $occFrcs * $adrFrcs;
                // REVRAR MTD BUDGET
                $revParBdg  = $occBdg * $adrBdg;

                // TOTAL REVENUE FCST
                $totalRevenueFrcs = Day::totalRevenue($id, $lD, 'fcstmtd');
                // TOTAL REVENUE MTD BUDGET
                $totalRevenueBdg  = Day::totalRevenue($id, $lD, 'budgetmtd');

                // ROOMS REVENUE
                $roomsRevenue     = Day::roomRevenue($id, $lD, 'mtd');
                // ROOMS REVENUE FCST
                $roomsRevenueFrcs = Day::roomRevenue($id, $lD, 'fcstmtd');
                // ROOMS REVENUE MTD BUDGET
                $roomsRevenueBdg  = Day::roomRevenue($id, $lD, 'budgetmtd');

                // F&B REVENUE
                $fBRevenue     = Day::fBRevenue($id, $lD, 'mtd');
                // F&B REVENUE FCST
                $fBRevenueFrcs = Day::fBRevenue($id, $lD, 'fcstmtd');
                // F&B REVENUE MTD BUDGET
                $fBRevenueBdg  = Day::fBRevenue($id, $lD, 'budgetmtd');

                // CONFERENCE REVENUE
                $conferenceRevenue     = Day::conferenceRevenue($id, $lD, 'mtd');
                // CONFERENCE REVENUE FCST
                $conferenceRevenueFrcs = Day::conferenceRevenue($id, $lD, 'fcstmtd');
                // CONFERENCE REVENUE MTD BUDGET
                $conferenceRevenueBdg  = Day::conferenceRevenue($id, $lD, 'budgetmtd');

                // OTHER REVENUE
                $otherRevenue     = Day::otherRevenue($id, $lD, 'mtd');
                // OTHER REVENUE FCST
                $otherRevenueFrcs = Day::otherRevenue($id, $lD, 'fcstmtd');
                // OTHER REVENUE MTD BUDGET
                $otherRevenueBdg  = Day::otherRevenue($id, $lD, 'budgetmtd');


                return view('pages.mtd', compact('lD', 'level', 'hotel', 'day', 'occ', 'adr', 'revPar', 'totalRevenue', 'laborEfficiency'
                                , 'occFrcs', 'adrFrcs', 'revParFrcs', 'totalRevenueFrcs', 'roomsRevenue', 'roomsRevenueFrcs', 'fBRevenue', 'fBRevenueFrcs'
                                , 'conferenceRevenue', 'conferenceRevenueFrcs', 'otherRevenue', 'otherRevenueFrcs', 'occBdg', 'adrBdg', 'revParBdg', 'roomsRevenueBdg', 'totalRevenueBdg', 'fBRevenueBdg', 'conferenceRevenueBdg', 'otherRevenueBdg', 'dates'));
            }
            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

    public function getMonthend()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('10:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }


            if (count($day) > 0 && count($level) > 0) {

                // RENT | RE_FORECAST | BUDGET
                $rent     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RE')->sum('fcst');
                $rentYes  = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RE')->sum('refcst');
                $rentBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RE')->sum('budget');

                // OCC | RE_FORECAST | BUDGET              
                $av     = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('fcst');
                $avYes  = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('refcst');
                $avBudg = Day::where('code', 'AV')->where('import_date', $lD)->where('hotel_id', $id)->sum('budget');


                $st = Day::occ($id, $lD, 'fcst');

                $occ = $av == '0' ? '0' : $st / $av;

                // OCC RE-FORECAST

                $stYes = Day::occ($id, $lD, 'refcst');

                if ($avYes == '0') {
                    $occYes = '0';
                } else {
                    $occYes = $stYes / $avYes;
                }

                // OCC BUDGET

                $stBudg = Day::occ($id, $lD, 'budget');

                if ($avBudg == '0') {
                    $occBudg = '0';
                } else {
                    $occBudg = $stBudg / $avBudg;
                }

                // ADR  | RE_FORECAST | BUDGET         

                $adr = $st == 0 ? '0' : Day::adr($id, $lD, 'fcst') / $st;

                $adrYes = $stYes == 0 ? '0' : Day::adr($id, $lD, 'refcst') / $stYes;

                $adrBudg = Day::adrBdg($id, $lD, 'budget');

                if ($adrBudg == 0) {
                    $adrBudg = 0;
                } else {
                    $adrBudg = $adrBudg / $stBudg;
                }

                // REVRAR | RE_FORECAST | BUDGET

                $revPar     = $occ * $adr;
                $revParYes  = $occYes * $adrYes;
                $revParBudg = $occBudg * $adrBudg;

                // TOTAL REVENUE                

                $totalRevenue = Day::totalRevenue($id, $lD, 'fcst');

                // TOTAL REVENUE RE_FORECAST

                $totalRevenueYes = Day::totalRevenue($id, $lD, 'refcst');

                // TOTAL REVENUE BUDGET                

                $totalRevenueBudg = Day::totalRevenue($id, $lD, 'budget');


                if ($totalRevenueBudg == 0) {
                    $totalRevenueBudg = 0;
                } else {
                    $totalRevenueBudg = Day::totalRevenue($id, $lD, 'budget');
                }

//                $lD = date_format(date_create($lD), "d-m-Y");
                return view('pages.monthend', compact('rent', 'rentYes', 'rentBudg', 'lD', 'level', 'hotel', 'day', 'occ', 'adr', 'revPar', 'totalRevenue', 'occYes', 'adrYes', 'revParYes', 'totalRevenueYes', 'occBudg', 'adrBudg', 'revParBudg', 'totalRevenueBudg', 'dates'));
            }
            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

    public function getRooms()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);

        if (count($hotel) > 0) {
            $day = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('20:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }

            if (count($day) > 0 && count($level) > 0) {
                // TRANSIENT | RE_FORECAST | BUDGET
                $transient     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RT')->sum('mtd');
                $transientFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RT')->sum('fcstmtd');
                $transientBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RT')->sum('budgetmtd');

                // GROUP | RE_FORECAST | BUDGET
                $group     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RG')->sum('mtd');
                $groupFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RG')->sum('fcstmtd');
                $groupBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RG')->sum('budgetmtd');

                // CONTRACT | RE_FORECAST | BUDGET
                $contract     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RC')->sum('mtd');
                $contractFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RC')->sum('fcstmtd');
                $contractBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'RC')->sum('budgetmtd');

                // OTHER REVENUE | RE_FORECAST | BUDGET
                $otherRevenueOR = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OR')->sum('mtd');
                $otherRevenueNS = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'NS')->sum('mtd');
                $otherRevenueCF = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CF')->sum('mtd');
                $otherRevenue   = $otherRevenueOR + $otherRevenueNS + $otherRevenueCF;

                $otherRevenueFcstOR = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OR')->sum('fcstmtd');
                $otherRevenueFcstNS = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'NS')->sum('fcstmtd');
                $otherRevenueFcstCF = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CF')->sum('fcstmtd');
                $otherRevenueFcst   = $otherRevenueFcstOR + $otherRevenueFcstNS + $otherRevenueFcstCF;

                $otherRevenueBudgOR = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'OR')->sum('budgetmtd');
                $otherRevenueBudgNS = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'NS')->sum('budgetmtd');
                $otherRevenueBudgCF = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CF')->sum('budgetmtd');
                $otherRevenueBudg   = $otherRevenueBudgOR + $otherRevenueBudgNS + $otherRevenueBudgCF;



//                $lD = date_format(date_create($lD), "d-m-Y");
                return view('pages.rooms', compact('lD', 'level', 'hotel', 'day', 'transient', 'group', 'contract', 'otherRevenue', 'transientFcst', 'groupFcst', 'contractFcst', 'otherRevenueFcst', 'transientBudg', 'groupBudg', 'contractBudg', 'otherRevenueBudg', 'dates'));
            }
            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

    public function getRestaurant()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('20:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }

            if (count($day) > 0 && count($level) > 0) {
                // BREAKFAST | RE_FORECAST | BUDGET
                $breakfast     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PB')->sum('mtd');
                $breakfastFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PB')->sum('fcstmtd');
                $breakfastBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PB')->sum('budgetmtd');

                // LUNCH | RE_FORECAST | BUDGET
                $lunch     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PL')->sum('mtd');
                $lunchFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PL')->sum('fcstmtd');
                $lunchBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PL')->sum('budgetmtd');

                // DINER | RE_FORECAST | BUDGET
                $diner     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PD')->sum('mtd');
                $dinerFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PD')->sum('fcstmtd');
                $dinerBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PD')->sum('budgetmtd');

                // FOOD OTHER | RE_FORECAST | BUDGET
                $food     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PO')->sum('mtd');
                $foodFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PO')->sum('fcstmtd');
                $foodBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'PO')->sum('budgetmtd');

                // BEVERAGE | RE_FORECAST | BUDGET
                $beverage     = Day::beverage($id, $lD, 'mtd');
                $beverageFcst = Day::beverage($id, $lD, 'fcstmtd');
                $beverageBudg = Day::beverage($id, $lD, 'budgetmtd');


//                $lD = date_format(date_create($lD), "d-m-Y");
                return view('pages.restaurant', compact('lD', 'level', 'hotel', 'day', 'breakfast', 'lunch', 'diner', 'food', 'beverage', 'breakfastFcst', 'lunchFcst', 'dinerFcst', 'foodFcst', 'beverageFcst', 'breakfastBudg', 'lunchBudg', 'dinerBudg', 'foodBudg', 'beverageBudg', 'dates'));
            }
            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

    public function getConference()
    {
        $id    = Session::get('hotel');
        $hotel = Hotel::find($id);
        if (count($hotel) > 0) {
            $level = AddUserToHotel::where('hotel_id', $id)->where('user_id', Auth::user()->id)->first();
            $day   = Day::where('hotel_id', $id)->get();
            if (count($day) < 1) {
                return redirect('user/home');
            }
            $lD    = Day::lastDay($id);
            $dates = Day::allDates($id);
            $date  = \Input::get('date');
            if ($date) {
                $lD = \Input::get('date');
            }
            // CHECK CURRENT DATE
            $curentDate = strtotime(date('d-m-Y'));
            $curentTime = time();
            $time       = strtotime('10:00:00');
            if (strtotime($lD) == $curentDate && $curentTime < $time && count(Day::allDates($id)) > 1) {
                $lD = Day::secondHighest($id);
            }

            if (count($day) > 0 && count($level) > 0) {
                // BANQUETS | RE_FORECAST | BUDGET
                $banquets     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CB')->sum('mtd');
                $banquetsFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CB')->sum('fcstmtd');
                $banquetsBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CB')->sum('budgetmtd');

                // ROOM RENTAL | RE_FORECAST | BUDGET
                $roomRental     = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CR')->sum('mtd');
                $roomRentalFcst = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CR')->sum('fcstmtd');
                $roomRentalBudg = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CR')->sum('budgetmtd');

                // OTHER CONFERENCE | RE_FORECAST | BUDGET
                $otherConferenceCA = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CA')->sum('mtd');
                $otherConferenceCO = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CO')->sum('mtd');
                $otherConference   = $otherConferenceCA + $otherConferenceCO;

                $otherConferenceFcstCA = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CA')->sum('fcstmtd');
                $otherConferenceFcstCO = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CO')->sum('fcstmtd');
                $otherConferenceFcst   = $otherConferenceFcstCA + $otherConferenceFcstCO;

                $otherConferenceBudgCA = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CA')->sum('budgetmtd');
                $otherConferenceBudgCO = Day::where('hotel_id', $id)->where('import_date', $lD)->where('code', 'CO')->sum('budgetmtd');

                $otherConferenceBudg = $otherConferenceBudgCA + $otherConferenceBudgCO;


//                $lD = date_format(date_create($lD), "d-m-Y");
                return view('pages.conference', compact('lD', 'level', 'hotel', 'day', 'banquets', 'roomRental', 'otherConference'
                                , 'banquetsFcst', 'roomRentalFcst', 'otherConferenceFcst', 'banquetsBudg', 'roomRentalBudg', 'otherConferenceBudg', 'dates'));
            }
            return view('pages.empty', compact('hotel', 'day'));
        } else {
            return redirect('user/home');
        }
    }

    public function download()
    {

        $file = public_path() . '/ipa/app.plist';
        if (file_exists($file)) {
            $headers = array(
                'Content-Type: application/octet-stream',
                'Content-Type: application/download',
                'Content-Disposition: attachment; filename="app.plist"',
                'Location: itms-services://?action=download-manifest&url=http://www.marriot.mar/app/app.plist'
            );
            return Response::download($file, false, $headers);
        }
    }

    public function downloadApp()
    {

        $file = public_path() . '/ipa/Marriot.ipa';
        if (file_exists($file)) {
            $headers = array(
                'Content-Type: application/octet-stream',
                'Content-Type: application/download',
                'Content-Disposition: attachment; filename="Marriot.IPA"',
            );
            return Response::download($file, false, $headers);
        }
    }

}
