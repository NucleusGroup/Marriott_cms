<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use App\User;
use Response;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ApiController extends Controller {

    public function postLogin() {

        $rules = [
            'email' => 'required|email', 'password' => 'required',
        ];

        $v = Validator::make(Input::all(), $rules);
        if ($v->fails()) {
            return Response::json(array(
                        'Validation Errors' => 'Email and Password are required',
            ), 422);
        }
        $email = Input::get('email');
        $password = Input::get('password');
        
        $userdata = array(
            'email' => $email,
            'password' => $password,
        );
        
        $key = User::where('email', $email)->first();
        // attempt to do the login
        if (Auth::attempt($userdata)) {
            return Response::json(array(
                        'success' => 'User loged in',
                        'key' => $key->confirmation_code
            ), 200);
        }

        return Response::json(array(
                    'Errors' => 'There was error processing your login'
        ), 401);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
