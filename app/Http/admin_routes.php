<?php

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');

    #Admin Dashboard
    Route::get('dashboard', 'Admin\DashboardController@index');

    #Language
    Route::get('language', 'Admin\LanguageController@index');
    Route::get('language/create', 'Admin\LanguageController@getCreate');
    Route::post('language/create', 'Admin\LanguageController@postCreate');
    Route::get('language/{id}/edit', 'Admin\LanguageController@getEdit');
    Route::post('language/{id}/edit', 'Admin\LanguageController@postEdit');
    Route::get('language/{id}/delete', 'Admin\LanguageController@getDelete');
    Route::post('language/{id}/delete', 'Admin\LanguageController@postDelete');
    Route::get('language/data', 'Admin\LanguageController@data');
    Route::get('language/reorder', 'Admin\LanguageController@getReorder');


    #Upload
    Route::get('uploads', 'Admin\UploadController@index');
    Route::get('upload/create', 'Admin\UploadController@getCreate');
    Route::post('upload/create', 'Admin\UploadController@postCreate');
    Route::get('upload/{id}/edit', 'Admin\UploadController@getEdit');
    Route::post('upload/{id}/edit', 'Admin\UploadController@postEdit');
    Route::get('upload/{id}/delete', 'Admin\UploadController@postDelete');
    Route::get('upload/{id}/delete-file', 'Admin\UploadController@postDeleteImport');
    Route::get('upload/{id}/import', 'Admin\UploadController@import');
//    Route::post('upload/{id}/delete', 'Admin\UploadController@postDelete');
    Route::post('file-uploads', 'Admin\UploadController@uploadFiles');
    
    Route::get('upload/data', 'Admin\UploadController@data');
    
    
     # HOTEL
    Route::get('hotels', 'Admin\HotelController@index');
    Route::get('hotel/create', 'Admin\HotelController@create');
    Route::post('hotel/create', 'Admin\HotelController@store');
    Route::get('hotel/{id}/edit', 'Admin\HotelController@edit');
    Route::post('hotel/{id}/edit', 'Admin\HotelController@update');
    Route::get('hotel/{id}/delete', 'Admin\HotelController@destroy');
    Route::get('upload/{id}/delete-file', 'Admin\UploadController@postDeleteImport');
    Route::get('upload/{id}/import', 'Admin\UploadController@import');
//    Route::post('upload/{id}/delete', 'Admin\UploadController@postDelete');
    Route::post('file-uploads', 'Admin\UploadController@uploadFiles');
    
    Route::get('hotel/data', 'Admin\HotelController@data');



    #Users
    Route::get('users/', 'Admin\UserController@index');
    Route::get('users/create', 'Admin\UserController@getCreate');
    Route::post('users/create', 'Admin\UserController@postCreate');
    Route::get('users/{id}/edit', 'Admin\UserController@getEdit');
    Route::post('users/{id}/edit', 'Admin\UserController@postEdit');
    Route::get('users/{id}/delete', 'Admin\UserController@postDelete');
    
    Route::get('users/hotel-delete/{id}','Admin\UserController@deleteHotel');
    
    
//    Route::post('users/{id}/delete', 'Admin\UserController@postDelete');
    Route::get('users/data', 'Admin\UserController@data');
});
