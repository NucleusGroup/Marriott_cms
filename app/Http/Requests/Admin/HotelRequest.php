<?php

namespace App\Http\Requests\Admin;
use Input;
use Illuminate\Foundation\Http\FormRequest;

class HotelRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $rules = [
            'name' => 'required',
//            'description' => 'required',
            'image' => 'required|image|image_size:>=1500,200-900',
            'sqr' => 'numeric',
//            'openingDate' => 'required',
//            'city' => 'required',
//            'number_of_rooms' => 'required',
//            'meeting_rooms' => 'required',
            'link' => 'url',
        ];
        
        $image = Input::get('hid');
       
        if(!empty($image)) {            
            unset($rules['image']);
        }
        
        return $rules;
    }
    
    public function messages() {
        
        return [
            'link.url' => 'The link format is invalid. Please use the following example. http://www.example.com',

        ];
        
    }

}
