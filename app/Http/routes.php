<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::model('hotel', 'App\Hotel');

Route::get('/', 'Auth\AuthController@getLogin');
Route::get('app/download', 'HomeController@download' );
//Route::get('ios-download', 'HomeController@downloadApp' );

Route::group(['prefix' => 'user', 'middleware' => ['auth', 'user', 'confirm']], function() {
    Route::get('key-figures-yesterday', 'HomeController@getStats');
    Route::get('revenue-details-yesterday', 'HomeController@getRevenue');
    Route::get('on-the-books', 'HomeController@getDiagram');
    Route::get('month-to-date', 'HomeController@getMtd');
//    Route::group(array('middleware' => 'level'), function() {
        Route::get('full-month-total', 'HomeController@getMonthend');
        Route::get('mtd-room-and-other', 'HomeController@getRooms');
        Route::get('mtd-restaurant', 'HomeController@getRestaurant');
        Route::get('mtd-conference', 'HomeController@getConference');
//    });

    Route::get('diagram', 'HomeController@getMdt');
    Route::get('stats/{hotel}', 'HomeController@redirect');
//    Route::get('diagram/{hotel}', 'HomeController@redirectMtd');
    Route::get('home', 'HomeController@index');
});
//Route::get('about', 'PagesController@about');
//Route::get('contact', 'PagesController@contact');

Route::pattern('id', '[0-9]+');
//Route::get('news/{id}', 'ArticlesController@show');
//Route::get('video/{id}', 'VideoController@show');
//Route::get('photo/{id}', 'PhotoController@show');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

if (Request::is('admin/*')) {
    require __DIR__ . '/admin_routes.php';
}

Route::group(array('prefix' => 'api/v1', 'middleware' => 'auth'), function() {
    Route::group(array('prefix' => 'users'), function () {

        Route::match(array('get', 'put', 'patch', 'delete'), 'login', function () {

            return Response::json(array(
                        'Method Not Allowed' => 405
            ));
        });

        Route::post('login', "ApiController@postLogin");
    });

// hotel    
    Route::get('hotels', "HotelController@index");
    Route::get('hotels/{hotel}', "HotelController@show");
});


