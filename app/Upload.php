<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Upload extends Model{

    protected $table = 'upload';

    protected $fillable = [ 'name'];
    /**
     * Deletes a xls file.
     *
     * @return bool
     */
    public function delete()
    {
       // Delete the gallery image
        return parent::delete();
    }

    public static function findDateAndHotel($id, $date) {
        $fileDate = Upload::where('hotel_id', $id)->where('import_date', $date)->first();
        if($fileDate) {
            return $fileDate;
        }
        return null;
    }
    
    
}