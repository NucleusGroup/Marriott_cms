<header>

    <div class="col-sm-1 logo"> 
        <ul class="nav nav-tabs">
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bars fa-2x"></i></a>

                <ul class="dropdown-menu">
                    @if(isset($day))
                    <?php $date = isset($_GET['date']) ? $_GET['date'] : null; ?>
                    <li><a {{ (Request::is('user/key-figures-yesterday') ? ' class=active_menu' : '') }} href="{{url('/user/key-figures-yesterday')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>Key Figures Yesterday</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/revenue-details-yesterday') ? ' class=active_menu' : '') }} href="{{url('/user/revenue-details-yesterday')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>Revenue Details Yesterday</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/on-the-books') ? ' class=active_menu' : '') }} href="{{url('/user/on-the-books')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>On The Books</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/month-to-date') ? ' class=active_menu' : '') }} href="{{url('/user/month-to-date')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>Month to Date</div></a>

                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/full-month-total') ? ' class=active_menu' : '') }} href="{{url('/user/full-month-total')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>Full Month Total</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/mtd-room-and-other') ? ' class=active_menu' : '') }} href="{{url('/user/mtd-room-and-other')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>MTD Room & Other</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/mtd-restaurant') ? ' class=active_menu' : '') }} href="{{url('/user/mtd-restaurant')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>MTD Restaurant</div></a>
                    <li class="divider"></li>
                    <li><a {{ (Request::is('user/mtd-conference') ? ' class=active_menu' : '') }} href="{{url('/user/mtd-conference')}}<?php if($date): echo  "?date=" . $date; endif ?>"><div>MTD Conference</div></a>
                    <li class="divider"></li>
                    @endif
                    <li>
                        <div class="headline" @if(!isset($day)) id="sm-menu" @endif>
                             <a href="{{url('user/home')}}"> <i class="fa fa-home"><span> Home</span></i></a>           
                            <a href="{{url('auth/logout')}}"> <i class="fa fa-sign-out"><span> Logout</span></i></a>
                        </div>         
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="col-sm-10">
        <h1 class="title">{{$hotel->name}}</h1>
    </div>


    <div class="col-sm-1">
        <div class="headlineIcon">
            <a href="#"><img data-toggle="modal" data-target="#myModal" src="{{asset('assets/site/images/info-icon.png')}}" alt="info"></a>
        </div>
    </div>
</header>
