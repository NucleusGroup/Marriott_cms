@if($hotel && isset($hotel))
<!-- MODAL -->
<?php
$date = date_create($hotel->opening_date);
$date = date_format($date, 'd-m-Y');
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Information about the hotel</h4>
            </div>
            <div class="modal-body">
                <p>Country: <strong>{{ $hotel->country }}</strong></p>
                <p>City: <strong>{{ $hotel->city }}</strong></p>
                <p>Number of rooms: <strong>{{ $hotel->number_of_rooms }}</strong></p>
                <p>SQM: <strong>{{ $hotel->sqr_mtr }}</strong></p>
                <p>Meeting rooms: <strong>{{ $hotel->meeting_rooms }}</strong></p>
                <p>Year Build: <strong>{{ $date }}</strong></p>
                <p><a class="link" href="{{ $hotel->link }}">Link to reservation page</a></p>
            </div>
        </div>
    </div>
</div>
@endif