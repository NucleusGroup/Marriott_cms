@if(isset($dates))
@if(count($dates) > 1)
<form id="dateForm" method="GET">
    <select id="dates" name="date" class="selectpicker" data-live-search="true">
        @foreach($dates as $date) 
        <option  @if($lD == $date->import_date ) selected @endif value="{{$date->import_date}}">{{date_format(date_create($date->import_date), "d-m-Y")}}</option>
        @endforeach
    </select>
</form> 
@endif
@endif