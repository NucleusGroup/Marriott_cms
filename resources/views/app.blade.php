<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <title>@section('title') Nordic Hospitality Performance @show</title>
        @section('meta_keywords')
        <meta name="keywords" content="Nordic Hospitality Performance, hotel"/>
        @show @section('meta_author')
        <meta name="author" content="extraordinary"/>
        @show @section('meta_description')
        <meta name="description"
              content="Nordic Hospitality Performance"/>
        @show

        <link href="{{ asset('/css/all.css') }}" rel="stylesheet">
        {{--<link href="{{elixir('css/all.css')}}" rel="stylesheet">--}}

        {{-- TODO: Incorporate into elixer workflow. --}}
        <link rel="stylesheet"
              href="{{asset('assets/site/css/half-slider.css')}}">
        <link rel="stylesheet"
              href="{{asset('assets/site/css/justifiedGallery.min.css')}}"/>
        <link rel="stylesheet"
              href="{{asset('assets/site/css/lightbox.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/site/css/bootstrap-theme.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/admin/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/site/css/responsive.css')}}">
        @if(Request::is('admin/*'))
        <link rel="stylesheet" href="{{asset('assets/site/css/main.css')}}">
        @else
        <link rel="stylesheet" href="{{asset('assets/site/css/page.css')}}">
        <link rel="stylesheet" href="{{asset('assets/site/css/jquery.typeahead.css')}}">
        <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-select.css')}}">
        @if(isset($hotel))
        @if(Request::is('user/home'))
        <?php $filePath = '/assets/site/images/login-background.png'; ?>
        @else
        <?php $filePath = '/appfiles/hotel/' . $hotel->id . '/main_image_blur.jpg'; ?>        
        @endif
        <link rel="stylesheet" href="/assets/site/css/style.php?path=<?php echo $filePath; ?>">
        @endif
        @endif



        @yield('styles')

        <!-- Fonts -->
        <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--<link rel="shortcut icon" href="{{{ asset('assets/site/ico/favicon.ico') }}}">-->

    </head>
    <body>


        @include('flash::message')
        <div class="container">
            @if(Auth::user() && Auth::user()->id == '1')
            @include('partials.nav')
            @endif
            @yield('content')
        </div>
        <!-- FOOTER -->


        <!-- Scripts -->
        <script src="{{ asset('/js/all.js') }}"></script>
        {{--<script src="{{ elixir('js/all.js') }}"></script>--}}

    {{-- TODO: Incorporate into elixir workflow. --}}
    <script src="{{asset('assets/site/js/jquery.justifiedGallery.min.js')}}"></script>
    <script src="{{asset('assets/site/js/lightbox.min.js')}}"></script>


    <script>
$('#flash-overlay-modal').modal();
$('div.alert').not('.alert-danger').delay(3000).slideUp(300);
    </script>

    @yield('scripts')

</body>
</html>
