@extends('admin.layouts.default')
{{-- Web site Title --}}
@section('title') {{{ trans("admin/users.users") }}} :: @parent @stop

@section('content')
@include('notifications')
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">{{{
			trans('admin/modal.general') }}}</a></li>
</ul>
<form class="form-horizontal" method="post"
      action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif"
      autocomplete="on">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab-general">
            <br/>

            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">{{
						trans('admin/users.name') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="1"
                               placeholder="{{ trans('admin/users.name') }}" type="text"
                               name="name" id="name"
                               value="{{{ Input::old('name', isset($user) ? $user->name : null) }}}">
                        {!! $errors->first('name', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>
            @if(!isset($user))
            <!--            <div class="col-md-12">
                            <div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
                                <label class="col-md-2 control-label" for="username">{{
                                                            trans('admin/users.username') }}</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="username" tabindex="4"
                                           placeholder="{{ trans('admin/users.username') }}" name="username"
                                           id="username"
                                           value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}" />
                                    {!! $errors->first('username', '<label class="control-label"
                                                                           for="username">:message</label>')!!}
                                </div>
                            </div>
                        </div>-->
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="email">{{
						trans('admin/users.email') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="email" tabindex="4"
                               placeholder="{{ trans('admin/users.email') }}" name="email"
                               id="email"
                               value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
                        {!! $errors->first('email', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="password">{{
						trans('admin/users.password') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" tabindex="5"
                               placeholder="{{ trans('admin/users.password') }}"
                               type="password" name="password" id="password" value="" />
                        {!!$errors->first('password', '<span class="error help-block">:message</span>')!!}
                        <a href="#" class="link-password" id="generate">Generate password</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="password_confirmation">{{
						trans('admin/users.password_confirmation') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="password" tabindex="6"
                               placeholder="{{ trans('admin/users.password_confirmation') }}"
                               name="password_confirmation" id="password_confirmation" value="" />
                        {!!$errors->first('password_confirmation', '<span class="error help-block">:message</span>')!!}
                    </div>
                </div>
            </div>

            <!-- LEVEL -->

            @if(isset($hotelsUser) && count($hotelsUser) > 0)

            <?php $i = 1; ?>
            <div id="plus" class="input_fields_wrap"> 
                @foreach($hotelsUser as $hotelUser)

                <div class="old col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="level">{{
						trans('admin/users.level') . ' ' . $i}}</label>                  
                        <div class="col-md-6">
                            <select class="form-control" name="hotel[]" id="level">
                                <?php $hotel = App\Hotel::find($hotelUser->hotel_id); ?>
                                <?php if($hotel): ?>
                                <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                                <?php else: ?>
                                @foreach($hotels as $hotel)
                                <option value="{{$hotel->id}}" @if(\Input::old('hotel') && in_array($hotel->id, \Input::old('hotel')) ) selected @endif>{{$hotel->name}}</option>
                                @endforeach
                                <?php endif; ?>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <select class="form-control" name="level[]" id="level">
                                @foreach($levels as $level)
                                <option @if($hotelUser->level_id == $level->id) selected @endif value="{{$level->id}}">{{$level->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <a href="{{url('admin/users/hotel-delete/' . $hotelUser->id)}}" class="btn btn-sm btn-danger delete"><span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                </div>

                <?php $i++; ?>
                @endforeach
            </div>
            @else

            <div id="plus" class="input_fields_wrap">                

                <div class="old col-md-12">

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="hotel">{{trans('admin/users.level')}} 1</label>                    

                        <div class="col-md-6 {{{ $errors->has('hotel') ? 'has-error' : '' }}}">
                            <select class="form-control" name="hotel[]" id="level">
                                <option disabled="" selected ></option>
                                @foreach($hotels as $hotel)
                                <option value="{{$hotel->id}}" @if(\Input::old('hotel') && in_array($hotel->id, \Input::old('hotel')) ) selected @endif>{{$hotel->name}}</option>
                                @endforeach
                            </select>
                            {!!$errors->first('hotel', '<span class="error help-block">:message</span>')!!}
                        </div>

                        <div class="col-md-3 {{{ $errors->has('level') ? 'has-error' : '' }}}">
                            <select class="form-control" name="level[]" id="level">
                                <option disabled="" selected ></option>
                                @foreach($levels as $level)                                
                                <option  value="{{$level->id}}" @if(\Input::old('level') && in_array($level->id, \Input::old('level')) ) selected @endif>{{$level->name}}</option>
                                @endforeach

                            </select>
                            {!!$errors->first('level', '<span class="error help-block">:message</span>')!!}
                        </div>
                        <!--<button class="btn btn-sm btn-info add_field_button"><span class="glyphicon glyphicon-plus-sign"></span></button>-->
                    </div>

                </div>
            </div>

            @endif
            <!-- CONFIRM -->
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('confirmeds') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="confirm">{{
						trans('admin/users.activate_user') }}</label>
                    <div class="col-md-6">
                        <select class="form-control" name="confirmed" id="confirmed">
                            <option value="1" {{{ ((isset($user) && $user->confirmed == 1 )? '
                                selected="selected"' : '') }}}>{{{ trans('admin/users.yes')
                                }}}</option>
                            <option value="0" {{{ ((isset($user) && $user->confirmed == 0 ) ?
                                ' selected="selected"' : '') }}}>{{{ trans('admin/users.no')
                                }}}</option>
                        </select>
                    </div>
                </div>
            </div>

            <!-- SEND EMAIL -->
            <div class="col-md-12 push">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="email">Send password via email to the user?</label>
                    <div class="col-md-10">
                        <input class="check" @if(\Input::old('sendEmail')) checked="" @endif type="checkbox" name="sendEmail"> 
                    </div>
                </div>
            </div>



        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6">
            <a href="{{ URL::to('admin/users') }}" class="btn btn-sm btn-warning close_popup">
                <span class="glyphicon glyphicon-ban-circle"></span> {{
				trans("admin/modal.cancel") }}
            </a>

            <button type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span> 
                @if	(isset($user))
                {{ trans("admin/modal.edit") }}
                @else
                {{trans("admin/modal.create") }}
                @endif
            </button>
        </div>
        <div class="pull-right">
            <div class="col-md-6">
                <button class="btn  btn-info add_field_button"><span class="glyphicon glyphicon-plus-sign"></span> Add hotel</button>
            </div>
        </div>
    </div>

</form>
@stop @section('scripts')
<script type="text/javascript" src="{{asset('assets/admin/js/deleteConf.js')}}"></script>
<script type="text/javascript">
$(document).ready(function () {
        var max_fields = {{ count($hotels) }};
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID 
        var n = $(".old").length;
            var count = {{ count($hotels) }};
            if (n == count) {
               $(".add_field_button").attr('disabled', true);
                    
            } else {
               $(add_button).attr('disabled', false);
            }       
        
        var x = 1; //initlal text box count

        $(add_button).click(function (e) { //on add input button click
            var n = $(".old").length;
            var count = {{ count($hotels) }};
            if (n == count) {
                    $(this).attr('disabled', true);
                    return false;
            }
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                var html = "<div class='old col-md-12'><div class='form-group'><label class='col-md-2 control-label' for='level'>Hotel | Level " + (n + 1);
                    html += "</label><div class='col-md-6'><select class='form-control' name='hotel[]' >@foreach($hotels as $hotel)";
                    html += "<option value='{{$hotel->id}}'>{{$hotel->name}}</option>@endforeach</select></div>";
                    html += "<div class='col-md-3'><select class='form-control' name='level[]' >";
                    html += "<?php foreach ($levels as $level): ?><option value='<?php echo $level->id; ?>'><?php echo preg_replace('/[\r\n]+/', null, $level->name); ?></option><?php endforeach; ?></select></div></label>";
                    html += "<a href='#' class='btn btn-sm btn-danger remove_field'><span class='glyphicon glyphicon-trash'></span></a></div></div>";
                    $(wrapper).append(html); //add input box
                    x++; //text box increment
            }
        });
        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
if (confirm('Are you sure you want to delete this item?')) {
e.preventDefault();
        $(this).parent().remove();
        x--;
}
return false;
})
        });</script>
<script src="{{asset('assets/admin/js/password-generate.js')}}" type="text/javascript"></script>

@stop
