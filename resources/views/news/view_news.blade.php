<p style="text-align: center;">Le Groupe O2i recherche des candidats de tous horizons pour s&rsquo;investir dans une entreprise dynamique.</p>
<p style="text-align: center;">Esprit d&rsquo;&eacute;quipe, engagement, sens du client sont des valeurs fortes au sein du groupe.</p>
<p style="text-align: center;">Pour nous rejoindre, d&eacute;couvrez nos offres d&rsquo;emplois et de stages et d&eacute;posez votre candidature.</p>
<div style="text-align: center;">&nbsp;</div>
<div style="text-align: center;">&nbsp;</div>
<div class="col-sm-12"><img class="careers-img" src="img/careers-img.png" alt="" />
    <div class="references invest-references careers-references">
        <div class="row">
            <div class="col-sm-4">
                <div class="references-info">
                    <div class="scroll answerform" style="text-align: left;">
                    <p><strong>Poste assistance commerciale et administrative H/F</strong></p>
                    <p class="scroll answerform" style="text-align: left;"><br />Contrat : CDI</p>
                    <p>Poste : situ&eacute; &agrave; Lyon</p>
                    <p>Dur&eacute;e hebdomadaire : 35 heures.</p>
                    <p style="text-align: left;font-size: 14px">Travaillant en bin&ocirc;me sous la responsabilit&eacute; d&rsquo;un(e) conseiller(e) formation, votre mission sera de fid&eacute;liser nos clients existants et d&eacute;velopper nos cibles de prospection sont les grands comptes, les SSII, les TPE, PME ainsi que les administrations. Pour cela, vous principales missions seront :</p>
                    <ul style="text-align: left;">
                        <li>- Accompagnement commercial : devis, suivi de commandes,</li>
                        <li>- Gestion administrative et logistique des inscriptions,</li>
                        <li>- Mise &agrave; jour, int&eacute;grit&eacute;, qualification des donn&eacute;es Navision (contacts, devis, stagiaires, commandes, sessions, &hellip;),</li>
                        <li>- Prospection t&eacute;l&eacute;phonique,</li>
                        <li>- Relation clients de toute nature et accompagnement de commerciaux &agrave; des rendez-vous clients,</li>
                        <li>- Gestion des Compte rendu d&rsquo;audit de connaissance et de satisfaction,</li>
                        <li>- Relances commerciales de toutes natures (retour de questionnaire, relance de planification&hellip;),</li>
                        <li>- Am&eacute;nagement de salles (transfert des machines lors des journ&eacute;es de grosses influences&hellip;).</li>
                        <li>- Plus diverses missions en fonction des besoins du bin&ocirc;me ou de l&rsquo;agence (accueil, standard, &hellip;)</li>
                    </ul>
                    <p style="text-align: left;font-size: 14px">Une exp&eacute;rience r&eacute;ussie dans un poste similaire (id&eacute;alement dans la formation professionnelle) d'au moins 2 ans, un bon relationnel et l'esprit d'&eacute;quipe, une bonne maitrise des outils bureautique et si possible de Navision. <br /> Votre go&ucirc;t pour le travail d&rsquo;&eacute;quipe, la prospection, votre sens de l'&eacute;coute, votre t&eacute;nacit&eacute; et votre capacit&eacute; &agrave; g&eacute;rer des projets d'envergure nationale vous permettront de r&eacute;ussir dans vos futures missions. Salaire : Motivant + variable. Tickets Restaurant + mutuelle + GSM.</p>
                    </div>
                    <div class="scroll answerform" style="text-align: left;"><em>Envoi de votre candidature &agrave;&nbsp;</em>: <a href="mailto:carrieres@groupeO2i.com">carrieres@groupeO2i.com</a>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="references-info">
                    <div class="scroll answerform" style="text-align: left;">
                        <p><strong>Poste conseiller Formation H/F</strong></p>
                        <br />Contrat : CDI<br />
                        <p>Poste : situ&eacute; &agrave; Grenoble</p>
                        <p>Dur&eacute;e hebdomadaire : 35 heures.</p>
                        Votre mission sera de proposer une large gamme de solutions de formation r&eacute;partie dans 4 domaines :
                        <ul style="text-align: left;">
                            <li>- Formation des utilisateurs de l'informatique, formation des experts informaticiens.</li>
                            <li>- Formation des professionnels du multim&eacute;dia.</li>
                            <li>- Formation pour les dessinateurs CAO, DAO, CFAO.</li>
                            <li>- Formation en d&eacute;veloppement personnel et en management.</li>
                        </ul>
                        <span style="text-align: left;"> Vos cibles de prospection sont les grands comptes, les SSII, les TPE, PME ainsi que les administrations. Vous devrez, dans le cadre de vos fonctions, principalement d&eacute;marcher les responsables de formation, ressources humaines, les responsables informatiques, les services de communication et les bureaux d'&eacute;tudes. </span> <br /> <span style="text-align: left;"> Une exp&eacute;rience r&eacute;ussie dans la vente de services (id&eacute;alement dans la formation professionnelle) d'au moins 2 ans, et un plus. Un bon relationnel et l'esprit d'&eacute;quipe, une bonne culture informatique est un plus. Votre go&ucirc;t pour la prospection, votre sens de l'&eacute;coute, votre t&eacute;nacit&eacute; et votre capacit&eacute; &agrave; g&eacute;rer des projets d'envergure nationale vous permettront de r&eacute;ussir dans vos futures missions. Salaire motivant + variable. Tickets Restaurant + mutuelle + GSM. </span></div>
                    <div class="answerform" style="text-align: left;"><em>Envoi de votre candidature &agrave;&nbsp;</em>: <a href="mailto:carrieres@groupeO2i.com">carrieres@groupeO2i.com</a>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="references-info">
                    <div class="scroll answerform" style="text-align: left;">
                        <p><strong>Poste t&eacute;l&eacute;prospecteur H/F</strong></p>
                        <br />Contrat : CDI<br />
                        <p>Poste : situ&eacute; &agrave; Grenoble</p>
                        <p>Dur&eacute;e hebdomadaire : 35 heures.</p>
                        Votre mission sera de d&eacute;marcher nos clients et prospects afin de prendre rendez-vous pour les commerciaux afin qu&rsquo;ils proposent notre large gamme de solutions de formation r&eacute;partie dans 4 domaines :
                        <ul style="text-align: left;">
                            <li>- Formation des utilisateurs de l'informatique, formation des experts informaticiens.</li>
                            <li>- Formation des professionnels du multim&eacute;dia.</li>
                            <li>- Formation pour les dessinateurs CAO, DAO, CFAO.</li>
                            <li>- Formation en d&eacute;veloppement personnel et en management.</li>
                        </ul>
                        Vos cibles de prospection sont les grands comptes, les SSII, les TPE, PME ainsi que les administrations. Vous devrez, dans le cadre de vos fonctions, principalement d&eacute;marcher les responsables de formation, ressources humaines, les responsables informatiques, les services de communication et les bureaux d'&eacute;tudes. <br /> Une exp&eacute;rience r&eacute;ussie dans la t&eacute;l&eacute;prospection et/ou la vente de services (id&eacute;alement dans la formation professionnelle) d'au moins 2 ans, et un plus. Un bon relationnel et l'esprit d'&eacute;quipe, une bonne culture informatique est un plus. Votre go&ucirc;t pour la prospection, votre sens de l'&eacute;coute, votre t&eacute;nacit&eacute; et votre capacit&eacute; &agrave; g&eacute;rer des projets d'envergure nationale vous permettront de r&eacute;ussir dans vos futures missions. Salaire motivant + variable. Tickets Restaurant + mutuelle + GSM.</div>
                    <div class="scroll answerform" style="text-align: left;"><em>Envoi de votre candidature &agrave;&nbsp;</em>: <a href="mailto:carrieres@groupeO2i.com">carrieres@groupeO2i.com</a>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="references-info">
                    <div class="scroll answerform" style="text-align: left;">
                        <p><strong>Poste formateur bureautique H/F</strong></p>
                        <br />Contrat : CDD<br />
                        <p>Poste : situ&eacute; &agrave; Lyon (possible d&eacute;placements sur Grenoble)</p>
                        <p>Dur&eacute;e hebdomadaire : 35 heures.</p>
                        <p>Du 07/12/15 au 18/12/15. (15 semaines)</p>
                        Votre mission sera principalement l&rsquo;animation de sessions bureautique sur les logiciels du Pack Office pour nos clients (dans nos locaux M2i ou chez nos clients) Formation des utilisateurs du niveau d&eacute;butant &agrave; expert. La connaissance de certains produits multim&eacute;dia est un plus. Votre go&ucirc;t pour l&rsquo;animation de groupe ou de cours particulier, votre sens de l'&eacute;coute sont primordiales pour r&eacute;ussir dans cette mission. Salaire motivant + variable. Tickets Restaurant + mutuelle + GSM.</div>
                    <div class="scroll answerform" style="text-align: left;"><em>Envoi de votre candidature &agrave;&nbsp;</em>: <a href="mailto:carrieres@groupeO2i.com">carrieres@groupeO2i.com</a>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                        <div class="scroll answerform" style="text-align: left;">
                            <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>