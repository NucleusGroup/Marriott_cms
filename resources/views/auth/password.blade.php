@extends('login')

@section('content')
<div class="loginPage">
    <div class="container">

        <div class="row">
            <div class="col-md-12 logo">
                <p>SCANDINAVIAN<span>HOSPITALITY</span>
                <p class="management">MANAGEMENT</p>

            </div>
            <div class="col-md-12">
                <h1>Reset Password</h1>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form class="form-horizontal log-in" method="POST" action="{!! URL::to('/password/email') !!}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if (session('status'))

                    <label class="control-label authSuccess" for="email">{{ session('status') }}</label>

                    @endif
                    <div class="form-group">
                        <div class="col-md-12">
                        <h6>Email address</h6>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        {!! $errors->first('email', '<span class="help-block auth">:message</span>')!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn">
                            <i class="fa fa-long-arrow-right fa-3x"></i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-12 mail">
                <p><a href="{{url('/')}}"><i class="fa fa-home fa-3x"></i></a></p>
            </div>
        </div>
    </div>
</div>

<!--    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('errors.list')

                        <form class="form-horizontal" role="form" method="POST" action="{!! URL::to('/password/email') !!}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
@endsection
