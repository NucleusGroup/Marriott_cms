@extends('login')

@section('content')

<div class="loginPage">
    <div class="container">

        <div class="row">
            <div class="col-md-12 logo">
                <p>SCANDINAVIAN<span>HOSPITALITY</span>
                <p class="management">MANAGEMENT</p>

            </div>
            <div class="col-md-12">
                <h1>Reset Password</h1>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form class="log-in" method="POST" action="{!! URL::to('/password/reset') !!}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">


                        <div class="col-md-12">
                            <h6>Email address</h6>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            {!! $errors->first('email', '<span class="help-block auth">:message</span>')!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h6>Password</h6>
                            <input type="password" class="form-control" name="password">
                            {!! $errors->first('password', '<span class="help-block auth">:message</span>')!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <h6>Confirm Password</h6>
                            <input type="password" class="form-control" name="password_confirmation">
                            {!! $errors->first('password_confirmation', '<span class="help-block auth">:message</span>')!!}
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn">
                            <i class="fa fa-long-arrow-right fa-3x"></i>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-12 mail">
                <p><a href="{{url('/')}}"><i class="fa fa-home fa-3x"></i></a></p>
            </div>
        </div>
    </div>
</div>

<!--    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">

                        @include('errors.list')

                        <form class="form-horizontal" role="form" method="POST" action="{!! URL::to('/password/reset') !!}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Reset Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
@endsection
