@extends('login')

{{-- Web site Title --}}
@section('title') {{{ trans('site/user.login') }}} :: @parent @stop

{{-- Content --}}
@section('content')

<div class="loginPage">
    @if(!$mobile) 
    @if(\Agent::is('iPhone') || \Agent::is('iPad')) 
    <div class="alert alert-warning alert-block">
        <div class="container app">
            <h4> There is an application available for your device. Do you want to download it?</h4>

            <!--        <h4><a class="btn app-btn" href="/app/download"  role="button" >Yes</a> </h4>-->

            <h4><a class="btn app-btn" href="itms-services://?action=download-manifest&url=https://webyourservice.nl/enterprise-apps/ipa/app.plist"  role="button" >Yes</a> </h4>

            <h4><a class="btn app-btn" data-dismiss="alert" href="#"  role="button">No</a></h4>

        </div>
    </div>
    @else

    @if(\Agent::isAndroidOS())
    <div class="alert alert-warning alert-block">
        <div class="container app">
            <h4> There is an application available for your device. Do you want to download it?</h4>

            <!--        <h4><a class="btn app-btn" href="/app/download"  role="button" >Yes</a> </h4>-->
            <h4><a class="btn app-btn" href="https://webyourservice.nl/enterprise-apps/ipa/nhp.php"  role="button" >Yes</a> </h4>
            <h4><a class="btn app-btn" data-dismiss="alert" href="#"  role="button">No</a></h4>

        </div>
    </div>
    @endif
    @endif
    @endif


    <div class="container">



        <div class="col-md-12 logo">
            <p>SCANDINAVIAN<span>HOSPITALITY</span>
            <p class="management">MANAGEMENT</p>

        </div>
        <div class="col-md-12">
            <h1>Nordic Hospitality Performance</h1>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form class="form-horizontal log-in" method="POST" action="{!! URL::to('/auth/login') !!}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <h6>Email address</h6>
                    <input class="form-control" type="text" placeholder="Email" name="email">
                    {!! $errors->first('email', '<span class="help-block auth">:message</span>')!!}
                </div>
                <div class="form-group">
                    <h6>Password</h6>
                    <input class="form-control" type="password" placeholder="Password" name="password">
                    {!! $errors->first('password', '<span class="help-block auth">:message</span>')!!}
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input id="checkbox" type="checkbox" name="remember" > Remember Me
                        </label>
                    </div>
                </div>
                <div align="center" class="form-group">
                    <button type="submit" class="btn btnSubmit">
                        <i class="fa fa-long-arrow-right fa-3x"></i>
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-12 mail">
            <p>To receive login credentials send an email to:<br>
                <a href="mailto:rosie.elzas@marriott.com">rosie.elzas@marriott.com</a></p>
        </div>
        <div class="footer">
            <div class="col-md-12">
                <p><a href="{{url('/password/email')}}">Forgot password</a></p></div>
        </div>
    </div>
</div>
</div>
<!-- Scripts -->
<script src="{{ asset('/js/all.js') }}"></script>
{{--<script src="{{ elixir('js/all.js') }}"></script>--}}
@endsection
