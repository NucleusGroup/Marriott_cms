@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <!-- MAIN HOTEL -->
    <div class="mainHotel">
        <div class="wrap-content">
            <div class="col-md-12">
                <div class="pull-left">
                    <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
                </div>
                <h6>Key Figures Yesterday</h6>
                @include('partials/date-select')
            </div>
        </div>
        <!-- MAIN HOTEL CONTENT -->
        <div class="mainHotel-content">
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Occ %</p>
                </div>
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">
                    <p>{{round($occ*100)}}%</p>
                </div>
                 
                <div class="col-md-4 layer-4">                    
                    @if ($occ < $occFcst)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($occ > $occFcst)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    
                </div>
                 @endif
            </div>
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>ADR</p>
                </div>
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">
                    <p>{{ round($adr) }}</p>
                </div>
                
                <div class="col-md-4 layer-4">
                          
                    @if ($adr < $adrFcst)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($adr > $adrFcst)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    
                </div>
                @endif
            </div>
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>RevPar</p>
                </div>
                 @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">
                    <p>{{ number_format(round($revPar)) }}</p>
                </div>
               
                <div class="col-md-4 layer-4">
                   
                    @if ($revPar < $revParFcst)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($revPar > $revParFcst)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    
                </div>
                @endif
            </div>
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Transient Pickup</p>
                </div>
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">
                    <p>{{ number_format(round($transientPickup))}}</p>
                </div>                
                <div class="col-md-4 layer-4">
                    
                   
                    @if ($transientPickup < 0)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($transientPickup > 0)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @elseif ($transientPickup == 0)
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    
                </div>
                @endif
            </div>
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Group Pickup</p>
                </div>
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">
                    <p>{{ number_format(round($groupPickup)) }}</p>
                </div>
                <div class="col-md-4 layer-4">
                            
                    @if ($groupPickup < 0)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($groupPickup > 0)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @elseif($groupPickup == 0)
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    
                </div>
                @endif
            </div>
                        
            
            
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Total Revenue</p>
                </div>
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <div class="col-md-4 layer-3">      
                    <p>{{ number_format($totalRevenue) }}</p>
                </div>
                <div class="col-md-4 layer-4">
                   
                    @if ($totalRevenueDay < $totalRevenueFcst)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($totalRevenueDay > $totalRevenueFcst)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                   
                </div>
                @endif
            </div>
            @if($level->level_id == '2' || $level->level_id == '1')
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Labor efficiency</p>
                </div>
                <div class="col-md-4 layer-3">
                    <p>{{ round($laborEfficiency*100) }}%</p>
                </div>
                <div class="col-md-4 layer-4">
                    
                    @if ($d128 < $d126)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($d128 > $d126)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                   
                </div>
            </div>
            @endif
            @if($level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
            <div class="layer-one">
                <div class="col-md-4 layer-3">
                    <p>Rent</p>
                </div>
                <div class="col-md-4 layer-3">
                    <p>{{ number_format($rent) }}</p>
                </div>
                <?php /*
                <div class="col-md-4 layer-4">
                    @if(count($dayBefore)>0)
                    @if ($rent < $rentY)
                    <i class="fa fa-arrow-down fa-3x"></i>
                    @elseif ($rent > $rentY)
                    <i class="fa fa-arrow-up fa-3x"></i>
                    @else
                    <i class="fa fa-arrow-right fa-3x"></i>
                    @endif                    
                    @endif
                </div>
                 * */ ?>
                 
            </div>
            @endif
            
        </div>
        <!-- END MAIN HOTEL CONTENT -->
    </div> 
    <!-- END MAIN HOTEL -->
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif
</div> <!-- END ROW -->


@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function(){
   $('#dateForm').submit(); 
});
</script>
@endsection
@stop
