@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <div class="wrap-content">
        <div class="col-md-12">
            <div class="pull-left">
                <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
            </div>
            <h6>Full Month Total</h6>
             @include('partials/date-select')
        </div>
    </div>
    <div class="wrap">
        <div class="col-md-4 layer-1">

        </div>
        <div class="col-md-3 layer-2">
            <p>Forecast</p>
        </div>
        <div class="col-md-3 layer-3 re-forecast">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>re-Forecast</p>
            @endif
        </div>
        <div class="col-md-2 layer-4">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>Budget</p>
            @endif
        </div>
    </div>
    <div class="main-content">
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Occ %</p>
            </div>
            
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($occ*100))}}%</p>
                @endif
            </div>
            
           
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($occYes*100))}}%</p>
                 @endif
            </div>
           
            
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($occBudg*100))}}%</p>
                @endif
            </div>
            
        </div>
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>ADR</p>
            </div>
            
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adr))}}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adrYes))}}</p>
                @endif
            </div>
            
            <div class="col-md-2 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adrBudg))}}</p>
                @endif
            </div>
            
        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>RevPar</p>
            </div>
           
            <div class="col-md-3 layer-2">
                 @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revPar))}}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revParYes))}}</p>
                @endif
            </div>
           
            <div class="col-md-2 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revParBudg))}}</p>
                @endif
            </div>
            
        </div>



        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Total Revenue</p>
            </div>
           
            <div class="col-md-3 layer-2">
                 @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenue)}}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                 @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenueYes)}}</p>
                @endif
            </div>
            
            <div class="col-md-2 layer-4">
                 @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenueBudg)}}</p>
                @endif
            </div>
            
        </div>
        @if($level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Rent</p>
            </div>
            
            <div class="col-md-8 layer-2">
                @if($level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{ number_format($rent) }}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{ number_format($rentYes) }}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{ number_format($rentBudg) }}</p>
                @endif
            </div>
            
        </div>
        @endif

    </div>
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif

</div>



@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function(){
   $('#dateForm').submit(); 
});
</script>
@endsection
@stop
