@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <div class="wrap-content">
        <div class="col-md-12">
            <div class="pull-left">
                <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
            </div>
            <h6>MTD Conference</h6>
            @include('partials/date-select')
        </div>
    </div>
    <div class="wrap">
        <div class="col-md-4 layer-1">
          
        </div>
        <div class="col-md-3 layer-2">
            <p>Actual MTD</p>
        </div>
         
        <div class="col-md-3 layer-3">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>Fcst MTD</p>
            @endif
        </div>
        <div class="col-md-2 layer-4">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>Budget MTD</p>
            @endif
        </div>
         
    </div>
    <div class="main-content">
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Banquets F&B</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($banquets))}}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
               <p>{{number_format(round($banquetsFcst))}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($banquetsBudg))}}</p>
                 @endif
            </div>
            
        </div>
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Room Rental</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($roomRental))}}</p>
                 @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
               <p>{{number_format(round($roomRentalFcst))}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
               <p>{{number_format(round($roomRentalBudg))}}</p>
                @endif
            </div>
           
        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Other Conference</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($otherConference))}}</p>
                @endif
            </div>
            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
               <p>{{number_format(round($otherConferenceFcst))}}</p>
               @endif
            </div>
            <div class="col-md-2 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($otherConferenceBudg))}}</p>
                @endif
            </div>
            
        </div>

    </div>
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif

</div>



@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function(){
   $('#dateForm').submit(); 
});
</script>
@endsection
@stop
