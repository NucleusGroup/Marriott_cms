@extends('app')
@section('title') Home :: @parent @stop
@section('content')

<div class="row">
    <div class="hotel-menu">
        <div class="page-header-home">
            <div class="col-md-12 title">
                <h2>Nordic Hospitality Performance</h2>
                <!--<a href="{{url('auth/logout')}}" class="logout"><span class="glyphicon glyphicon-log-out"></span>LOGOUT</a>-->
            </div>

        </div>
        @if(Auth::user())
        @if(count($hotels) > 0)

        @foreach($hotels as $hotel)
        <div class="contentHome">

            <div class="col-md-12 waterfront">
                <a href="{{ url('user/stats' , $hotel->id) }}"><img src="{{asset('appfiles/hotel/' . $hotel->id .  '/' . $hotel->image)}}" alt="gdynia"/><h2>{{$hotel->name}}</h2></a>
            </div>

        </div>

        @endforeach
        @else 
        <small>There are no hotels</small>
        @endif
        @endif     
    </div>
</div>

@endsection

@section('scripts')
@parent

@endsection
@stop
