@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <!-- MAIN HOTEL -->
    <div class="mainHotel">
        <div class="mainHotel-content">
            <div class="wrap-content content-heading">
                <div class="col-md-12">
                    <div class="pull-left">
                        <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
                    </div>
                    <h6>Revenue Details Yesterday</h6>
                     @include('partials/date-select')
                </div>
            </div>
            @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
            <table> 
                <tbody>
                    <tr>
                        <td>
                            <p>Rooms</p>
                        </td>  
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Transient</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($transient))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Group</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($group))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Contract</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($contract))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Room Other</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($roomsOther))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Total rooms</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($totalRooms))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Restaurant</p>
                        </td>                        
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Breakfast</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($breakfast))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Lunch</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($lunch))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Dinner</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($dinner))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Food Other</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($foodOther))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Non Alcohol Beverage</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($nab))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Beer</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($beer))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Wine</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($wine))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Alcohol Beverage</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($liqour))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Total Restaurant</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($totalRestaurant))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Conference</p>
                        </td>                        
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Banquets</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($banquets))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Room Rental</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($roomRental))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Other Conference</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($otherConference))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Total Conference</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($totalConference))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Other Revenue</p>
                        </td>                        
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Merchandise</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Mini Market</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($miniMarket))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Laundry</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($laundry))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Parking</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($parking))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Other</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($other))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Total Other</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($totalOther))}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                <li>
                                    <p>Total Revenue</p>
                                </li>
                            </ul>
                        </td>
                        <td>
                            <p>{{number_format(round($totalRevenue2))}}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            @endif
        </div>

    </div> 
    <!-- END MAIN HOTEL -->
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif
</div> <!-- END ROW -->


@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function(){
   $('#dateForm').submit(); 
});
</script>
@endsection
@stop
