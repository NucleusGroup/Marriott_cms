@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <div class="wrap-content">
        <div class="col-md-12">
            <div class="pull-left">
                <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
            </div>
            <h6>MTD Restaurant</h6>
            @include('partials/date-select')
        </div>
    </div>
    <div class="wrap">
        <div class="col-md-4 layer-1">

        </div>
        <div class="col-md-3 layer-2">
            <p>Actual MTD</p>
        </div>

        <div class="col-md-3 layer-3">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>Fcst MTD</p>
            @endif
        </div>
        <div class="col-md-2 layer-4">
            @if($level->level_id == '2' || $level->level_id == '1')
            <p>Budget MTD</p>
            @endif
        </div>

    </div>
    <div class="main-content">
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Breakfast</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($breakfast)}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($breakfastFcst)}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($breakfastBudg)}}</p>
                @endif
            </div>

        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Lunch</p>
            </div>
            <div class="col-md-3 layer-2">
                <p>{{number_format($lunch)}}</p>
            </div>            
            <div class="col-md-3 layer-3">
                @if( $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($lunchFcst)}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($lunchBudg)}}</p>
                @endif
            </div>

        </div>
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Dinner</p>
            </div>
            <div class="col-md-3 layer-2">
                <p>{{number_format($diner)}}</p>
            </div>            
            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($dinerFcst)}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($dinerBudg)}}</p>
                @endif
            </div>

        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Food Other</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($food)}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($foodFcst)}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4"> 
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($foodBudg)}}</p>
                @endif
            </div>

        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Beverage</p>
            </div>
            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($beverage)}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($beverageFcst)}}</p>
                @endif
            </div>
            <div class="col-md-2 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($beverageBudg)}}</p>
                @endif
            </div>

        </div>

        @if($level->level_id == '1')
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Labor efficiency F&B</p>
            </div>
            <div class="col-md-3 layer-2">
                <p></p>
            </div>

            <div class="col-md-3 layer-3">

            </div>
            <div class="col-md-2 layer-4">
                <p></p>
            </div>

        </div>
        @endif


    </div>
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif

</div>



@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function () {
    $('#dateForm').submit();
});
</script>
@endsection
@stop
