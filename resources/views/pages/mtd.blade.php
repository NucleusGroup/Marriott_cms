@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))
    @include('partials.menu')
    <div class="wrap-content">
        <div class="col-md-12">
            <div class="pull-left">
                <h6><i>Last updated: {{date_format(date_create($lD), "d-m-Y")}}</i></h6>
            </div>
            <h6>Month to Date</h6>
            @include('partials/date-select')
        </div>
    </div>
    <div class="wrap">
        <div class="col-md-4 layer-1">

        </div>
        <div class="col-md-3 layer-2">
            <p>MTD</p>
        </div>
        <div class="col-md-3 layer-3">
             @if($level->level_id == '2' || $level->level_id == '1')
            <p>MTD FCST</p>
             @endif
        </div>
        <div class="col-md-2 layer-4">
             @if($level->level_id == '2' || $level->level_id == '1')
            <p>MTD Budget</p>
             @endif
        </div>
    </div>
    <div class="main-content">
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Occ %</p>
            </div>

            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{round($occ*100)}}%</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{round($occFrcs*100)}}%</p>
                @endif
            </div>

            <div class="col-md-3 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{round($occBdg*100)}}%</p>
                @endif
            </div>



        </div>
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>ADR</p>
            </div>

            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adr))}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adrFrcs))}}</p>
                @endif
            </div>            
            <div class="col-md-3 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($adrBdg))}}</p>
                @endif
            </div>


        </div>

        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>RevPar</p>
            </div>

            <div class="col-md-3 layer-2">
                @if($level->level_id == '4' || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revPar))}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revParFrcs))}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format(round($revParBdg))}}</p>
                @endif
            </div>


        </div>
        <?php /*
          <div class="layer-one">
          <div class="col-md-4 layer-1">
          <p>Rooms Revenue</p>
          </div>
          <div class="col-md-3 layer-2">
          <p>{{number_format(round($roomsRevenue,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-3">
          <p>{{number_format(round($roomsRevenueFrcs,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-4">
          <p>{{number_format(round($roomsRevenueBdg,2),2)}}</p>
          </div>

          </div>

          <div class="layer-one">
          <div class="col-md-4 layer-1">
          <p>F&B  Revenue</p>
          </div>
          <div class="col-md-3 layer-2">
          <p>{{number_format(round($fBRevenue,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-3">
          <p>{{number_format(round($fBRevenueFrcs,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-4">
          <p>{{number_format(round($fBRevenueBdg,2),2)}}</p>
          </div>

          </div>

          <div class="layer-one">
          <div class="col-md-4 layer-1">
          <p>Conference  Revenue</p>
          </div>
          <div class="col-md-3 layer-2">
          <p>{{number_format(round($conferenceRevenue,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-3">
          <p>{{number_format(round($conferenceRevenueFrcs,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-4">
          <p>{{number_format(round($conferenceRevenueBdg,2),2)}}</p>
          </div>

          </div>

          <div class="layer-one">
          <div class="col-md-4 layer-1">
          <p>Other  Revenue</p>
          </div>
          <div class="col-md-3 layer-2">
          <p>{{number_format(round($otherRevenue,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-3">
          <p>{{number_format(round($otherRevenueFrcs,2),2)}}</p>
          </div>
          <div class="col-md-3 layer-4">
          <p>{{number_format(round($otherRevenueBdg,2),2)}}</p>
          </div>

          </div>
         */ ?>

        <!-- TOTAL REVENUE -->
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Total Revenue</p>
            </div>

            <div class="col-md-3 layer-2">
                @if($level->level_id == '4'  || $level->level_id == '3' || $level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenue)}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-3">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenueFrcs)}}</p>
                @endif
            </div>

            <div class="col-md-3 layer-4">
                @if($level->level_id == '2' || $level->level_id == '1')
                <p>{{number_format($totalRevenueBdg)}}</p>
                @endif
            </div>


        </div>
        @if($level->level_id == '2' || $level->level_id == '1')
        <div class="layer-one">
            <div class="col-md-4 layer-1">
                <p>Labor efficiency</p>
            </div>
            <div class="col-md-3 layer-2">

                <p><span>{{round($laborEfficiency)*100}}%</span></p>

            </div>
            <div class="col-md-3 layer-3">

            </div>
            <div class="col-md-2 layer-4">   

            </div>
        </div>
        @endif

    </div>
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif

</div>



@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-select.js')}}"></script>
<script type="text/javascript">
$('.selectpicker').selectpicker();
$('#dates').change(function () {
    $('#dateForm').submit();
});
</script>
@endsection
@stop
