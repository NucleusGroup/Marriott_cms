@extends('app')
@section('title') {{isset($hotel) ? $hotel->name : '' }} :: @parent @stop
@section('content')

<div class="row">
    @if(isset($hotel))   

    <div class="col-md-12 heding">
        @include('partials.menu')

        <div class="graph">
            <div id="container" width='100%'></div>
        </div>

        <h6><i>Last updated: {{$lD}}</i></h6>

        <h2>On The Books</h2>

        <button id="dateBtn" type="button" class="btn btn-default" data-toggle="modal" data-target="#dateModal">
            <i class="fa fa-calendar-o"></i> Choose Date
        </button>

    </div>
    <div class="section-heading">
        <div class="col-xs-2 section">

            <p>OTB<br> Transient</p>

        </div>
        <div class="col-xs-2 section">
            <p class="group">Group</p>
        </div>
        <div class="col-xs-2 section">
            <p>% of Budget TY</p>
        </div>
        <div class="col-xs-2 section">
            <p>% of Actual LY</p>
        </div>
        <div class="col-xs-4 section-one">
            <h3>Chg from Yestrday</h3>
            <div class="col-xs-6 section-part">
                <p class="part">Transient</p>
            </div>
            <div class="col-xs-6 section-part-two">
                <p class="part-two">Group</p>
            </div>
        </div>
    </div>

    <?php
    $sumT = 0;
    $sumG = 0;    
    ?>
    <div class="main">
        @for($i = 1; $i <= 12; $i++)
        <div class="col-md-12 main-section">
            <div class="col-md-2 one">
                <?php
                switch ($i) {
                    case 1;
                        echo "<span>Jan</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 2;
                        echo "<span>Feb</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 3;
                        echo "<span>Mar</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 4;
                        echo "<span>Apr</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 5;
                        echo "<span>May</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 6;
                        echo "<span>Jun</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 7;
                        echo "<span>Jul</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 8;
                        echo "<span>Aug</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 9;
                        echo "<span>Sep</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 10;
                        echo "<span>Oct</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 11;
                        echo "<span>Nov</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                    case 12;
                        echo "<span>Dec</span><p>" . number_format($otbT[$i]) . "</p>";
                        break;
                }
                ?>
            </div>

            <div class="col-md-2 one">
                <p>{{number_format($otbG[$i])}}</p>
            </div>
            <div class="col-md-2 one">  
                @if($otbGBudget[$i] == 0 || $otbTBudget[$i] == 0)
                 <code>0%</code>
                @else
                 <code>{{ number_format((($otbG[$i] + $otbT[$i] )/($otbGBudget[$i] + $otbTBudget[$i]))*100) }}%</code>
                @endif
               
            </div>
            <div class="col-md-2 one">
                <p></p>
            </div>
            <div class="col-md-2 one">
                <p>{{$otbT[$i] - $otbTYes[$i]}}</p>
                <?php $sumT += ($otbT[$i] - $otbTYes[$i]); ?>
            </div>
            <div class="col-md-2 one">
                <p>{{$otbG[$i] - $otbGYes[$i]}}</p>
                <?php $sumG += ($otbG[$i] - $otbGYes[$i]); ?>
            </div>
        </div>
        @endfor
        <div class="col-md-12 main-section total-section">
            
            <div class="col-md-2 one">
               <strong> <span>Total</span></strong>
                <p><strong>{{number_format($otbTramsientSum)}}</strong></p>
            </div>

            <div class="col-md-2 one">
                <p><strong>{{number_format($otbGroupSum)}}</strong></p>
            </div>
            <div class="col-md-2 one">
                <strong><code>{{number_format($budget*100)}}%</code></strong>
            </div>
            <div class="col-md-2 one">
                <p></p>
            </div>
            <div class="col-md-2 one">
                <p><strong>{{$sumT}}</strong></p>
            </div>
            <div class="col-md-2 one">
                <p><strong>{{$sumG}}</strong></p>
            </div>
        </div>
    </div>
    @include('partials.modal')
    @else
    <div class="pull-right">
        <div class="pull-right">
            <a href="{{{ URL::to('/') }}}"
               class="btn btn-sm  btn-primary"><span
                    class="glyphicon glyphicon-backward"></span> {{
					trans("admin/admin.back") }}</a>
        </div>
    </div>
    @endif
    <!-- DATE Modal -->
    <div class="modal fade" id="dateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-calendar-o"></i> Type Date</h4>
                </div>
                <div class="modal-body">
                    <form id="form-country_v1" name="form-country_v1" method="get">
                        <div class="typeahead-container">
                            <div class="typeahead-field">

                                <span class="typeahead-query">
                                    <input id="country_v1-query" name="date" placeholder="Search" autocomplete="off" type="search">
                                </span>
                                <span class="typeahead-button">
                                    <button id="searchBt" type="submit">
                                        <i class="typeahead-search-icon"></i>
                                    </button>
                                </span>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$res = array();
$resY = array();
$date = array();
$fcst = array();
$reFcst = array();
$pickUp = array();
$actuals = array();
$lost = array();
foreach ($graphs as $graph) {
    array_push($res, intval($graph->actuals), intval($graph->yesterday));
    array_push($actuals, intval($graph->actuals));
    array_push($resY, intval($graph->yesterday));
    array_push($pickUp, intval($graph->pick_up));
    array_push($fcst, intval($graph->fcst));
    array_push($reFcst, intval($graph->re_fcst));
    array_push($lost, intval($graph->lost));

    $timestamp = strtotime($graph->date);
    array_push($date, date("d", $timestamp));
}
//   foreach($otb as $key => $value) {
//        $results = $value - $resY[$key];
//       array_push($cal, $results );
//    }
?>
<script>
    var graphArray = <?php echo isset($res) ? json_encode(array_filter($res)) : 0; ?>;
    var graphYArray = <?php echo isset($resY) ? json_encode($resY) : 0; ?>;
    var pickUpArray = <?php echo isset($pickUp) ? json_encode($pickUp) : 0; ?>;
    var dateArray = <?php echo isset($date) ? json_encode($date) : 0; ?>;
    var fcstArray = <?php echo isset($fcst) ? json_encode($fcst) : 0; ?>;
    var refcstArray = <?php echo isset($reFcst) ? json_encode($reFcst) : 0; ?>;
    var actualsArray = <?php echo isset($actuals) ? json_encode($actuals) : 0; ?>;
    var lostArray = <?php echo isset($lost) ? json_encode($lost) : 0; ?>;
</script>


@endsection

@section('scripts')
@parent
<script type="text/javascript" src="{{asset('assets/site/js/chart.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/diagram.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/jquery.typeahead.js')}}"></script>
<?php
$resDate = array();
foreach ($dates as $date) {
    array_push($resDate, date_format(date_create($date->import_date), "d-m-Y"));
}
?>
<script>
    $('#searchBt').attr('disabled', true);
    $('#country_v1-query').typeahead({
        minLength: 1,
        order: "desc",
        source: {
            data: <?php echo isset($resDate) ? json_encode($resDate) : 0; ?>
        },
        callback: {
            onInit: function (node) {
//                console.log('Typeahead Initiated on ' + node.selector);
            },
            onClickAfter: function (node, query, obj, objCount) {
                $('#searchBt').attr('disabled', false);

            }
        }

    });
//    $('#select-date').change(function () {
//        $("#select-date").submit();
//    });
</script>
@stop
