<?php

return [
    'settings' => 'Settings',
    'homepage' => 'View Homepage',
    'home' => 'Home',
    'welcome' => 'Welcome',
    'action' => 'Actions',
    'back' => 'Back',
    'created_at' => 'Created at',
    'import_date' => 'Import date',
    'language' => 'Language',
    'yes' => 'Yes',
    'no' => 'No',
    'view_detail' => 'View Details',
    'news_categories' => 'News categories',
    'hotel_items' => 'Hotels',
    'photo_albums' => 'Photo albums',
    'photo_items' => 'Photo items',
    'video_albums' => 'Video albums',
    'video_items' => 'Video items',
    'users' => 'Users',
    'hotel' => 'Hotel',
];
