<?php

return [
    'users' => 'Users',
    'name' =>   'Name',
    'username' =>   'Username',
    'email' =>   'Email',
    'password' =>  'Password',
    'password_confirmation' => 'Password confirmation',
    'activate_user' => 'Activate user',
    'active_user' => 'Active user',
    'level' => 'Hotel | Level',
    'yes' => 'Yes',
    'no' => 'No',
    'roles' => 'Roles',
    'roles_info' => 'Adding the role to user, be assigned all privileges for a given role.',
    'crud' => array(
        'create' => 'A new user has been sucessfully added.',
        'edit' => 'User has been updated.',
        'delete' => 'User has been deleted.',
        'hotel-delete' => 'Hotel has been removed from the list.',
        'duplicate' => 'You can not submit duplicate values!',
    ),

];
