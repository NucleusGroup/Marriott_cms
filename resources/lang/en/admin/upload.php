<?php

return [
    'uploads' => 'Files',
    'name'    => 'Name',
    'hotel'    => 'Choose Hotel',
    'status'  => 'Status',
    'upload_items'  => 'Upload items',
    'delete'  => 'File has been successfully deleted.',
    'import'  => 'File has been successfully imported.',
    'file_delete'  => 'File has been successfully deleted.',
];
