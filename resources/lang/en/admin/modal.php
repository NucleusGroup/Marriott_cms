<?php

return [
    'create' => 'Create',
    'edit' => 'Edit',
    'reset' => 'Reset',
    'cancel' => 'Cancel',
    'general' => 'General',
    'title' => 'Title',
    'new' => 'New',
    'delete' => 'Delete',
    'items' => 'Items',
    'delete_message' => 'Did you want to delete this item?',
    'import' => 'Import',
    'delete' => 'Delete',
    'import' => 'Import File',
    'name' => 'Hotel name',
    'content' => 'Content',
    'back' => 'Back',

];