<?php

return [
    'hotel' => 'Hotels',
    'name' => 'Name',
    'opening_date' => 'Build date',
    'description' => 'Description',
    'sqr_mtr' => 'Square meters',
    'date' => 'Build date',
    'country' => 'Country',
    'city' => 'City',
    'number_of_rooms' => 'Number of rooms',
    'meeting_rooms' => 'Meeting rooms',
    'link' => 'Link',
    'new' => 'Create a new hotel',
    'edit' => 'Edit hotel',
    'image' => 'Choose image hotel (min:1500x200px)',
    'create' => 'Hotel successfully created',
    'edit' => 'Hotel successfully updated',
    'delete' => 'Hotel successfully deleted',
];
